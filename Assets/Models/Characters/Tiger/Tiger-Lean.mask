%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Tiger-Lean
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Body
    m_Weight: 1
  - m_Path: cogJoint
    m_Weight: 1
  - m_Path: cogJoint/DYNtaiJoint1
    m_Weight: 0
  - m_Path: cogJoint/DYNtaiJoint1/DYNtaiJoint2
    m_Weight: 0
  - m_Path: cogJoint/DYNtaiJoint1/DYNtaiJoint2/DYNtaiJoint3
    m_Weight: 0
  - m_Path: cogJoint/DYNtaiJoint1/DYNtaiJoint2/DYNtaiJoint3/DYNtaiJoint4
    m_Weight: 0
  - m_Path: cogJoint/DYNtaiJoint1/DYNtaiJoint2/DYNtaiJoint3/DYNtaiJoint4/DYNtaiJoint5
    m_Weight: 0
  - m_Path: cogJoint/FKtaiJoint1
    m_Weight: 0
  - m_Path: cogJoint/FKtaiJoint1/FKtaiJoint2
    m_Weight: 0
  - m_Path: cogJoint/FKtaiJoint1/FKtaiJoint2/FKtaiJoint3
    m_Weight: 0
  - m_Path: cogJoint/FKtaiJoint1/FKtaiJoint2/FKtaiJoint3/FKtaiJoint4
    m_Weight: 0
  - m_Path: cogJoint/FKtaiJoint1/FKtaiJoint2/FKtaiJoint3/FKtaiJoint4/FKtaiJoint5
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint/LKneeJoint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint/LKneeJoint/LAnkleJoint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint/LKneeJoint/LAnkleJoint/LBallJoint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint/LKneeJoint/LAnkleJoint/LBallJoint/LToeJoint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint/LKneeJoint/LAnkleJoint/LBallJoint/LToeJoint/LToeInnerJoint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint/LKneeJoint/LAnkleJoint/LBallJoint/LToeJoint/LToeMiddle1Joint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint/LKneeJoint/LAnkleJoint/LBallJoint/LToeJoint/LToeMiddle2Joint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint/LKneeJoint/LAnkleJoint/LBallJoint/LToeJoint/LToeOuterJoint
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_FK/LKneeJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_FK/LKneeJoint_FK/LAnkleJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_FK/LKneeJoint_FK/LAnkleJoint_FK/LBallJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_FK/LKneeJoint_FK/LAnkleJoint_FK/LBallJoint_FK/LToeJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_IK/LKneeJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_IK/LKneeJoint_IK/LAnkleJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_IK/LKneeJoint_IK/LAnkleJoint_IK/LBallJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/LHipJoint_IK/LKneeJoint_IK/LAnkleJoint_IK/LBallJoint_IK/LToeJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint/RKneeJoint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint/RKneeJoint/RAnkleJoint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint/RKneeJoint/RAnkleJoint/RBallJoint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint/RKneeJoint/RAnkleJoint/RBallJoint/RToeJoint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint/RKneeJoint/RAnkleJoint/RBallJoint/RToeJoint/RToeInnerJoint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint/RKneeJoint/RAnkleJoint/RBallJoint/RToeJoint/RToeMiddle1Joint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint/RKneeJoint/RAnkleJoint/RBallJoint/RToeJoint/RToeMiddle2Joint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint/RKneeJoint/RAnkleJoint/RBallJoint/RToeJoint/RToeOuterJoint
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_FK/RKneeJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_FK/RKneeJoint_FK/RAnkleJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_FK/RKneeJoint_FK/RAnkleJoint_FK/RBallJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_FK/RKneeJoint_FK/RAnkleJoint_FK/RBallJoint_FK/RToeJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_IK/RKneeJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_IK/RKneeJoint_IK/RAnkleJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_IK/RKneeJoint_IK/RAnkleJoint_IK/RBallJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/RHipJoint_IK/RKneeJoint_IK/RAnkleJoint_IK/RBallJoint_IK/RToeJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1
    m_Weight: 1
  - m_Path: cogJoint/spineJoint1/spineJoint2
    m_Weight: 1
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3
    m_Weight: 1
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LIndexFingerJoint1
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LIndexFingerJoint1/LIndexFingerJoint2
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LIndexFingerJoint1/LIndexFingerJoint2/LIndexFingerJoint3
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LMiddleFingerJoint1
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LMiddleFingerJoint1/LMiddleFingerJoint2
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LMiddleFingerJoint1/LMiddleFingerJoint2/LMiddleFingerJoint3
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LRingFingerJoint1
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LRingFingerJoint1/LRingFingerJoint2
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LRingFingerJoint1/LRingFingerJoint2/LRingFingerJoint3
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LThumbJoint1
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LThumbJoint1/LThumbJoint2
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint/LElbowJoint/LForarmJoint/LWristJoint/LThumbJoint1/LThumbJoint2/LThumbJoint3
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint_FK/LElbowJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint_FK/LElbowJoint_FK/LForarmJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint_FK/LElbowJoint_FK/LForarmJoint_FK/LWristJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint_IK/LElbowJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint_IK/LElbowJoint_IK/LForarmJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/LClavicle/LShoulderJoint_IK/LElbowJoint_IK/LForarmJoint_IK/LWristJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint
    m_Weight: 1
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint/headJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint/headJoint/LEarJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint/headJoint/LEyeJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint/headJoint/MouthRootJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint/headJoint/MouthRootJoint/MouthLowerJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint/headJoint/MouthUpperJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint/headJoint/REarJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/neckJoint/headJoint/REyeJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RIndexFingerJoint1
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RIndexFingerJoint1/RIndexFingerJoint2
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RIndexFingerJoint1/RIndexFingerJoint2/RIndexFingerJoint3
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RMiddleFingerJoint1
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RMiddleFingerJoint1/RMiddleFingerJoint2
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RMiddleFingerJoint1/RMiddleFingerJoint2/RMiddleFingerJoint3
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RRingFingerJoint1
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RRingFingerJoint1/RRingFingerJoint2
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RRingFingerJoint1/RRingFingerJoint2/RRingFingerJoint3
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RThumbJoint1
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RThumbJoint1/RThumbJoint2
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint/RElbowJoint/RForarmJoint/RWristJoint/RThumbJoint1/RThumbJoint2/RThumbJoint3
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint_FK/RElbowJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint_FK/RElbowJoint_FK/RForarmJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint_FK/RElbowJoint_FK/RForarmJoint_FK/RWristJoint_FK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint_IK/RElbowJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint_IK/RElbowJoint_IK/RForarmJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/spineJoint1/spineJoint2/spineJoint3/RClavicle/RShoulderJoint_IK/RElbowJoint_IK/RForarmJoint_IK/RWristJoint_IK
    m_Weight: 0
  - m_Path: cogJoint/taiJoint1
    m_Weight: 0
  - m_Path: cogJoint/taiJoint1/taiJoint2
    m_Weight: 0
  - m_Path: cogJoint/taiJoint1/taiJoint2/taiJoint3
    m_Weight: 0
  - m_Path: cogJoint/taiJoint1/taiJoint2/taiJoint3/taiJoint4
    m_Weight: 0
  - m_Path: cogJoint/taiJoint1/taiJoint2/taiJoint3/taiJoint4/taiJoint5
    m_Weight: 0
  - m_Path: LEye
    m_Weight: 1
  - m_Path: REye
    m_Weight: 1
  - m_Path: RootMovementCtrl
    m_Weight: 0
  - m_Path: RootMovementCtrl/cogCtrl
    m_Weight: 0
