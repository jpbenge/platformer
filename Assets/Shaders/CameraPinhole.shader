// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:1,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:False,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:1,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:6,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:1,qpre:4,rntp:5,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:32759,y:33254,varname:node_2865,prsc:2|emission-4676-OUT;n:type:ShaderForge.SFN_TexCoord,id:4219,x:31009,y:33241,cmnt:Default coordinates,varname:node_4219,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Relay,id:8397,x:32034,y:33088,cmnt:Refract here,varname:node_8397,prsc:2|IN-4219-UVOUT;n:type:ShaderForge.SFN_Relay,id:4676,x:32662,y:33356,cmnt:Modify color here,varname:node_4676,prsc:2|IN-8106-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:4430,x:31009,y:33064,ptovrint:False,ptlb:MainTex,ptin:_MainTex,cmnt:MainTex contains the color of the scene,varname:node_9933,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:0da1c0d92cd418c4c8b9a6ebad46da97,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:7542,x:32256,y:33172,varname:node_1672,prsc:2,tex:0da1c0d92cd418c4c8b9a6ebad46da97,ntxv:0,isnm:False|UVIN-8397-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:8566,x:30986,y:33569,ptovrint:False,ptlb:Pinhole,ptin:_Pinhole,varname:node_8566,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1167c0a28d11119930004d8a4241aa39,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Blend,id:8106,x:32521,y:33356,varname:node_8106,prsc:2,blmd:1,clmp:True|SRC-7542-RGB,DST-3726-RGB;n:type:ShaderForge.SFN_Tex2d,id:3726,x:32373,y:33463,varname:node_3726,prsc:2,tex:1167c0a28d11119930004d8a4241aa39,ntxv:0,isnm:False|UVIN-2421-UVOUT,TEX-8566-TEX;n:type:ShaderForge.SFN_Panner,id:3424,x:31580,y:33290,varname:node_3424,prsc:2,spu:-0.5,spv:-0.5|UVIN-3401-OUT,DIST-7016-OUT;n:type:ShaderForge.SFN_Slider,id:4838,x:30930,y:33445,ptovrint:False,ptlb:Pinhole Amount,ptin:_PinholeAmount,varname:node_4838,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:100;n:type:ShaderForge.SFN_ScreenParameters,id:5508,x:30920,y:33782,varname:node_5508,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1820,x:31952,y:33463,varname:node_1820,prsc:2|A-3424-UVOUT,B-8905-OUT;n:type:ShaderForge.SFN_Append,id:8905,x:31742,y:33485,varname:node_8905,prsc:2|A-6452-OUT,B-6441-OUT;n:type:ShaderForge.SFN_Vector1,id:6441,x:31192,y:33620,varname:node_6441,prsc:2,v1:1;n:type:ShaderForge.SFN_Subtract,id:7016,x:31353,y:33444,varname:node_7016,prsc:2|A-4838-OUT,B-6441-OUT;n:type:ShaderForge.SFN_Multiply,id:3401,x:31379,y:33290,varname:node_3401,prsc:2|A-4219-UVOUT,B-4838-OUT;n:type:ShaderForge.SFN_Panner,id:2421,x:32180,y:33463,varname:node_2421,prsc:2,spu:1,spv:0|UVIN-1820-OUT,DIST-2030-OUT;n:type:ShaderForge.SFN_Multiply,id:6965,x:31797,y:33643,varname:node_6965,prsc:2|A-2307-OUT,B-3995-OUT;n:type:ShaderForge.SFN_Divide,id:6452,x:31125,y:33915,varname:node_6452,prsc:2|A-5508-PXW,B-5508-PXH;n:type:ShaderForge.SFN_Vector1,id:3995,x:31625,y:33738,varname:node_3995,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:9086,x:31448,y:33841,varname:node_9086,prsc:2|A-5508-PXW,B-6452-OUT;n:type:ShaderForge.SFN_Subtract,id:2307,x:31612,y:33609,varname:node_2307,prsc:2|A-5508-PXW,B-9086-OUT;n:type:ShaderForge.SFN_Divide,id:2030,x:31950,y:33766,varname:node_2030,prsc:2|A-6965-OUT,B-5508-PXW;proporder:4430-8566-4838;pass:END;sub:END;*/

Shader "Shader Forge/CameraPinhole" {
    Properties {
        _MainTex ("MainTex", 2D) = "black" {}
        _Pinhole ("Pinhole", 2D) = "black" {}
        _PinholeAmount ("Pinhole Amount", Range(0, 100)) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay+1"
            "RenderType"="Overlay"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZTest Always
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Pinhole; uniform float4 _Pinhole_ST;
            uniform float _PinholeAmount;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float2 node_8397 = i.uv0; // Refract here
                float4 node_1672 = tex2D(_MainTex,TRANSFORM_TEX(node_8397, _MainTex));
                float node_6452 = (_ScreenParams.r/_ScreenParams.g);
                float node_6441 = 1.0;
                float2 node_2421 = ((((i.uv0*_PinholeAmount)+(_PinholeAmount-node_6441)*float2(-0.5,-0.5))*float2(node_6452,node_6441))+(((_ScreenParams.r-(_ScreenParams.r*node_6452))*0.5)/_ScreenParams.r)*float2(1,0));
                float4 node_3726 = tex2D(_Pinhole,TRANSFORM_TEX(node_2421, _Pinhole));
                float3 emissive = saturate((node_1672.rgb*node_3726.rgb));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
