// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:1,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:False,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:1,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:6,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:1,qpre:4,rntp:5,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:31838,y:34841,varname:node_2865,prsc:2|emission-6233-OUT;n:type:ShaderForge.SFN_TexCoord,id:4219,x:30287,y:33236,cmnt:Default coordinates,varname:node_4219,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Relay,id:8397,x:30956,y:33237,cmnt:Refract here,varname:node_8397,prsc:2|IN-4219-UVOUT;n:type:ShaderForge.SFN_Relay,id:4676,x:31234,y:34940,cmnt:Modify color here,varname:node_4676,prsc:2|IN-1739-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:4430,x:31669,y:34021,ptovrint:False,ptlb:MainTex,ptin:_MainTex,cmnt:MainTex contains the color of the scene,varname:node_9933,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Desaturate,id:6233,x:31668,y:34939,varname:node_6233,prsc:2|COL-8886-OUT,DES-7245-OUT;n:type:ShaderForge.SFN_Time,id:7318,x:31092,y:32816,varname:node_7318,prsc:2;n:type:ShaderForge.SFN_Tex2dAsset,id:3535,x:30287,y:33047,ptovrint:False,ptlb:Wave Normal,ptin:_WaveNormal,varname:node_3535,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:fb6566c21f717904f83743a5a76dd0b0,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:349,x:30956,y:33024,varname:node_349,prsc:2,tex:fb6566c21f717904f83743a5a76dd0b0,ntxv:0,isnm:False|TEX-3535-TEX;n:type:ShaderForge.SFN_Panner,id:1440,x:31413,y:33189,varname:node_1440,prsc:2,spu:0.02,spv:0.05|UVIN-8397-OUT,DIST-2374-OUT;n:type:ShaderForge.SFN_Multiply,id:2374,x:31526,y:32692,varname:node_2374,prsc:2|A-4601-OUT,B-349-G;n:type:ShaderForge.SFN_Sin,id:4601,x:31291,y:32692,varname:node_4601,prsc:2|IN-7318-T;n:type:ShaderForge.SFN_Panner,id:3595,x:31652,y:33189,varname:node_3595,prsc:2,spu:0.04,spv:0.02|UVIN-1440-UVOUT,DIST-157-OUT;n:type:ShaderForge.SFN_Cos,id:4602,x:31506,y:32842,varname:node_4602,prsc:2|IN-3721-OUT;n:type:ShaderForge.SFN_Multiply,id:3721,x:31291,y:32854,varname:node_3721,prsc:2|A-7318-T,B-3261-OUT;n:type:ShaderForge.SFN_Vector1,id:3261,x:31092,y:32946,varname:node_3261,prsc:2,v1:1.4;n:type:ShaderForge.SFN_Multiply,id:157,x:31665,y:32842,varname:node_157,prsc:2|A-4602-OUT,B-349-R;n:type:ShaderForge.SFN_Set,id:8522,x:32147,y:33256,varname:u,prsc:2|IN-2513-R;n:type:ShaderForge.SFN_Set,id:6595,x:32139,y:33325,varname:v,prsc:2|IN-2513-G;n:type:ShaderForge.SFN_Set,id:6585,x:32147,y:33175,varname:uv,prsc:2|IN-2513-OUT;n:type:ShaderForge.SFN_Slider,id:1010,x:29635,y:33432,ptovrint:False,ptlb:Blur Amount,ptin:_BlurAmount,varname:node_1010,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Set,id:4672,x:30189,y:33431,varname:blurAmount,prsc:2|IN-7421-OUT;n:type:ShaderForge.SFN_Add,id:4344,x:31284,y:33411,varname:node_4344,prsc:2|A-5007-OUT,B-6611-OUT;n:type:ShaderForge.SFN_Get,id:5007,x:31088,y:33392,varname:node_5007,prsc:2|IN-4672-OUT;n:type:ShaderForge.SFN_Get,id:6611,x:31088,y:33447,varname:node_6611,prsc:2|IN-8522-OUT;n:type:ShaderForge.SFN_Append,id:9018,x:31455,y:33389,varname:node_9018,prsc:2|A-4344-OUT,B-4289-OUT;n:type:ShaderForge.SFN_Get,id:4289,x:31263,y:33348,varname:node_4289,prsc:2|IN-6595-OUT;n:type:ShaderForge.SFN_Add,id:1863,x:31299,y:33602,varname:node_1863,prsc:2|A-4484-OUT,B-6747-OUT;n:type:ShaderForge.SFN_Get,id:4484,x:31103,y:33583,varname:node_4484,prsc:2|IN-4672-OUT;n:type:ShaderForge.SFN_Get,id:6747,x:31103,y:33638,varname:node_6747,prsc:2|IN-6595-OUT;n:type:ShaderForge.SFN_Append,id:6369,x:31467,y:33562,varname:node_6369,prsc:2|A-8071-OUT,B-1863-OUT;n:type:ShaderForge.SFN_Get,id:8071,x:31278,y:33546,varname:node_8071,prsc:2|IN-8522-OUT;n:type:ShaderForge.SFN_Get,id:2479,x:31104,y:33853,varname:node_2479,prsc:2|IN-4672-OUT;n:type:ShaderForge.SFN_Get,id:3536,x:31104,y:33799,varname:node_3536,prsc:2|IN-8522-OUT;n:type:ShaderForge.SFN_Append,id:5125,x:31481,y:33771,varname:node_5125,prsc:2|A-6365-OUT,B-760-OUT;n:type:ShaderForge.SFN_Get,id:760,x:31294,y:33743,varname:node_760,prsc:2|IN-6595-OUT;n:type:ShaderForge.SFN_Get,id:1623,x:31104,y:34026,varname:node_1623,prsc:2|IN-4672-OUT;n:type:ShaderForge.SFN_Get,id:428,x:31104,y:33977,varname:node_428,prsc:2|IN-6595-OUT;n:type:ShaderForge.SFN_Append,id:8973,x:31498,y:33953,varname:node_8973,prsc:2|A-1560-OUT,B-8586-OUT;n:type:ShaderForge.SFN_Get,id:1560,x:31291,y:33940,varname:node_1560,prsc:2|IN-8522-OUT;n:type:ShaderForge.SFN_Subtract,id:6365,x:31312,y:33799,varname:node_6365,prsc:2|A-3536-OUT,B-2479-OUT;n:type:ShaderForge.SFN_Subtract,id:8586,x:31291,y:34003,varname:node_8586,prsc:2|A-428-OUT,B-1623-OUT;n:type:ShaderForge.SFN_Add,id:4837,x:32271,y:33584,varname:node_4837,prsc:2|A-7493-RGB,B-305-RGB,C-7286-RGB,D-7098-RGB;n:type:ShaderForge.SFN_Get,id:7274,x:30714,y:34343,varname:node_7274,prsc:2|IN-4672-OUT;n:type:ShaderForge.SFN_Get,id:3644,x:30430,y:34402,varname:node_3644,prsc:2|IN-4672-OUT;n:type:ShaderForge.SFN_Multiply,id:2550,x:30735,y:34396,varname:node_2550,prsc:2|A-3644-OUT,B-6420-OUT;n:type:ShaderForge.SFN_Vector1,id:6420,x:30451,y:34477,varname:node_6420,prsc:2,v1:-1;n:type:ShaderForge.SFN_Add,id:895,x:31423,y:34194,varname:node_895,prsc:2|A-2221-OUT,B-6872-OUT;n:type:ShaderForge.SFN_Get,id:2221,x:31134,y:34404,varname:node_2221,prsc:2|IN-6585-OUT;n:type:ShaderForge.SFN_Append,id:6872,x:30960,y:34187,varname:node_6872,prsc:2|A-7274-OUT,B-7274-OUT;n:type:ShaderForge.SFN_Append,id:96,x:30960,y:34327,varname:node_96,prsc:2|A-7274-OUT,B-2550-OUT;n:type:ShaderForge.SFN_Append,id:7210,x:30960,y:34448,varname:node_7210,prsc:2|A-2550-OUT,B-7274-OUT;n:type:ShaderForge.SFN_Append,id:5750,x:30960,y:34573,varname:node_5750,prsc:2|A-2550-OUT,B-2550-OUT;n:type:ShaderForge.SFN_Add,id:1406,x:31423,y:34331,varname:node_1406,prsc:2|A-96-OUT,B-2221-OUT;n:type:ShaderForge.SFN_Add,id:9696,x:31423,y:34463,varname:node_9696,prsc:2|A-2221-OUT,B-7210-OUT;n:type:ShaderForge.SFN_Add,id:2468,x:31423,y:34595,varname:node_2468,prsc:2|A-2221-OUT,B-5750-OUT;n:type:ShaderForge.SFN_Divide,id:1476,x:32541,y:33857,varname:node_1476,prsc:2|A-4837-OUT,B-766-OUT;n:type:ShaderForge.SFN_Vector1,id:766,x:32316,y:33877,varname:node_766,prsc:2,v1:4;n:type:ShaderForge.SFN_Set,id:7032,x:33067,y:34020,varname:outTex,prsc:2|IN-7933-OUT;n:type:ShaderForge.SFN_Get,id:1739,x:31028,y:34940,varname:node_1739,prsc:2|IN-7032-OUT;n:type:ShaderForge.SFN_ComponentMask,id:2513,x:31912,y:33173,varname:node_2513,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-3595-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:7493,x:32039,y:33412,varname:node_7493,prsc:2,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False|UVIN-9018-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Tex2d,id:305,x:32039,y:33541,varname:node_305,prsc:2,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False|UVIN-6369-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Tex2d,id:7286,x:32039,y:33675,varname:node_7286,prsc:2,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False|UVIN-5125-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Tex2d,id:7098,x:32039,y:33834,varname:node_7098,prsc:2,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False|UVIN-8973-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Tex2d,id:3422,x:32018,y:34201,varname:node_3422,prsc:2,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False|UVIN-895-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Tex2d,id:6767,x:32018,y:34332,varname:node_6767,prsc:2,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False|UVIN-1406-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Tex2d,id:9099,x:32018,y:34465,varname:node_9099,prsc:2,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False|UVIN-9696-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Tex2d,id:8279,x:32018,y:34592,varname:node_8279,prsc:2,tex:e890c99641c80204eb9982ad498b3e2a,ntxv:0,isnm:False|UVIN-2468-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_Add,id:8555,x:32264,y:34382,varname:node_8555,prsc:2|A-3422-RGB,B-6767-RGB,C-9099-RGB,D-8279-RGB;n:type:ShaderForge.SFN_Divide,id:8125,x:32541,y:34073,varname:node_8125,prsc:2|A-8555-OUT,B-7484-OUT;n:type:ShaderForge.SFN_Vector1,id:7484,x:32316,y:34096,varname:node_7484,prsc:2,v1:4;n:type:ShaderForge.SFN_Add,id:9725,x:32709,y:33953,varname:node_9725,prsc:2|A-1476-OUT,B-8125-OUT;n:type:ShaderForge.SFN_Divide,id:7933,x:32886,y:34020,varname:node_7933,prsc:2|A-9725-OUT,B-9870-OUT;n:type:ShaderForge.SFN_Vector1,id:9870,x:32709,y:34086,varname:node_9870,prsc:2,v1:2;n:type:ShaderForge.SFN_Vector1,id:8565,x:29792,y:33517,varname:node_8565,prsc:2,v1:500;n:type:ShaderForge.SFN_Divide,id:7421,x:29986,y:33431,varname:node_7421,prsc:2|A-1010-OUT,B-8565-OUT;n:type:ShaderForge.SFN_Set,id:2896,x:30524,y:33625,varname:tintCol,prsc:2|IN-663-RGB;n:type:ShaderForge.SFN_Get,id:2924,x:30841,y:35011,varname:node_2924,prsc:2|IN-2896-OUT;n:type:ShaderForge.SFN_Color,id:663,x:30304,y:33625,ptovrint:False,ptlb:Tint Color,ptin:_TintColor,varname:node_663,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.4352942,c2:0.5803922,c3:0.9882354,c4:1;n:type:ShaderForge.SFN_Slider,id:1548,x:30158,y:33798,ptovrint:False,ptlb:Desaturation,ptin:_Desaturation,varname:node_1548,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.25,max:1;n:type:ShaderForge.SFN_Set,id:3741,x:30487,y:33798,varname:desaturationAmount,prsc:2|IN-1548-OUT;n:type:ShaderForge.SFN_Get,id:7245,x:31493,y:34993,varname:node_7245,prsc:2|IN-3741-OUT;n:type:ShaderForge.SFN_DepthBlend,id:9019,x:30706,y:35126,varname:node_9019,prsc:2|DIST-8186-OUT;n:type:ShaderForge.SFN_Lerp,id:8886,x:31333,y:34971,varname:node_8886,prsc:2|A-4676-OUT,B-2924-OUT,T-9019-OUT;n:type:ShaderForge.SFN_Slider,id:8926,x:30158,y:33908,ptovrint:False,ptlb:Color Blend Distance,ptin:_ColorBlendDistance,varname:node_8926,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:50,max:100;n:type:ShaderForge.SFN_Set,id:4679,x:30487,y:33904,varname:blendDistance,prsc:2|IN-8926-OUT;n:type:ShaderForge.SFN_Get,id:8186,x:30499,y:35126,varname:node_8186,prsc:2|IN-4679-OUT;proporder:4430-3535-663-1010-1548-8926;pass:END;sub:END;*/

Shader "Shader Forge/CameraUnderWater" {
    Properties {
        [HideInInspector]_MainTex ("MainTex", 2D) = "white" {}
        _WaveNormal ("Wave Normal", 2D) = "bump" {}
        _TintColor ("Tint Color", Color) = (0.4352942,0.5803922,0.9882354,1)
        _BlurAmount ("Blur Amount", Range(0, 1)) = 0
        _Desaturation ("Desaturation", Range(0, 1)) = 0.25
        _ColorBlendDistance ("Color Blend Distance", Range(0, 100)) = 50
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay+1"
            "RenderType"="Overlay"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZTest Always
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _WaveNormal; uniform float4 _WaveNormal_ST;
            uniform float _BlurAmount;
            uniform float4 _TintColor;
            uniform float _Desaturation;
            uniform float _ColorBlendDistance;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 projPos : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
////// Lighting:
////// Emissive:
                float blurAmount = (_BlurAmount/500.0);
                float4 node_7318 = _Time;
                float3 node_349 = UnpackNormal(tex2D(_WaveNormal,TRANSFORM_TEX(i.uv0, _WaveNormal)));
                float2 node_2513 = ((i.uv0+(sin(node_7318.g)*node_349.g)*float2(0.02,0.05))+(cos((node_7318.g*1.4))*node_349.r)*float2(0.04,0.02)).rg;
                float u = node_2513.r;
                float v = node_2513.g;
                float2 node_9018 = float2((blurAmount+u),v);
                float4 node_7493 = tex2D(_MainTex,TRANSFORM_TEX(node_9018, _MainTex));
                float2 node_6369 = float2(u,(blurAmount+v));
                float4 node_305 = tex2D(_MainTex,TRANSFORM_TEX(node_6369, _MainTex));
                float2 node_5125 = float2((u-blurAmount),v);
                float4 node_7286 = tex2D(_MainTex,TRANSFORM_TEX(node_5125, _MainTex));
                float2 node_8973 = float2(u,(v-blurAmount));
                float4 node_7098 = tex2D(_MainTex,TRANSFORM_TEX(node_8973, _MainTex));
                float2 uv = node_2513;
                float2 node_2221 = uv;
                float node_7274 = blurAmount;
                float2 node_895 = (node_2221+float2(node_7274,node_7274));
                float4 node_3422 = tex2D(_MainTex,TRANSFORM_TEX(node_895, _MainTex));
                float node_2550 = (blurAmount*(-1.0));
                float2 node_1406 = (float2(node_7274,node_2550)+node_2221);
                float4 node_6767 = tex2D(_MainTex,TRANSFORM_TEX(node_1406, _MainTex));
                float2 node_9696 = (node_2221+float2(node_2550,node_7274));
                float4 node_9099 = tex2D(_MainTex,TRANSFORM_TEX(node_9696, _MainTex));
                float2 node_2468 = (node_2221+float2(node_2550,node_2550));
                float4 node_8279 = tex2D(_MainTex,TRANSFORM_TEX(node_2468, _MainTex));
                float3 outTex = ((((node_7493.rgb+node_305.rgb+node_7286.rgb+node_7098.rgb)/4.0)+((node_3422.rgb+node_6767.rgb+node_9099.rgb+node_8279.rgb)/4.0))/2.0);
                float3 tintCol = _TintColor.rgb;
                float blendDistance = _ColorBlendDistance;
                float desaturationAmount = _Desaturation;
                float3 emissive = lerp(lerp(outTex,tintCol,saturate((sceneZ-partZ)/blendDistance)),dot(lerp(outTex,tintCol,saturate((sceneZ-partZ)/blendDistance)),float3(0.3,0.59,0.11)),desaturationAmount);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
