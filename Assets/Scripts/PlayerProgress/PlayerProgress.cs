﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable()]
public class PlayerProgress: ISerializable {

	public Dictionary<string, LevelProgress> levelProgress;
	//Could use a bitmask here, or a dictionary or something
	public HashSet<string> learnedMoves;

	public PlayerProgress()
	{
		levelProgress = new Dictionary<string, LevelProgress>();
		learnedMoves = new HashSet<string>();
	}

	//Deserialization
	public PlayerProgress(SerializationInfo info, StreamingContext ctxt)
	{
		levelProgress = (Dictionary<string, LevelProgress>) info.GetValue("levelProgress", typeof(Dictionary<string, LevelProgress>));
		learnedMoves = new HashSet<string> ((List<string>) info.GetValue("learnedMoves", typeof(List<string>)));
	}

	//Serialization
	public void GetObjectData(SerializationInfo info, StreamingContext context)
	{
		info.AddValue("levelProgress", levelProgress);
		info.AddValue("learnedMoves", learnedMoves.ToList());
	}

	public void UnlockMove(string moveName)
	{
		learnedMoves.Add(moveName);
	}
}
