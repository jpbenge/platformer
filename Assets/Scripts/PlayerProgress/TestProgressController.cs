﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestProgressController : AbstractProgressController {

	void Awake()
	{
		if (state == null)
		{
			state = this;
			Setup();
		}
		else
		{
			Destroy(gameObject);
		}
	}

	private void Setup()
	{
		Profile = new PlayerProgress();
	}
}
