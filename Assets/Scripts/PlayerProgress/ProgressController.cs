﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProgressController : AbstractProgressController {

	private int doorwayID = -1;
	private List<GameObject> doorways;

	void OnEnable() {
		SceneManager.sceneLoaded += OnLevelLoad;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnLevelLoad;
	}

	void Awake () {
		if (state == null) {
			state = this;
			DontDestroyOnLoad(gameObject);
			LoadOrCreateState();
		}
		else {
			Destroy (gameObject);
		}
	}

	private void OnApplicationQuit()
	{
		SaveProfile();
	}

	public void Update() {
		InputManager.CheckControllerStatus ();
	}

	private void OnLevelLoad(Scene scene, LoadSceneMode mode) {
		player = GameObject.Find ("tiger");
		doorways = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Doorway"));
		doorways.Sort ((a, b) => {
			return a.GetComponent<DoorwayTrigger>().Id > b.GetComponent<DoorwayTrigger>().Id ? 1 : -1;
		});
		if (doorwayID >= 0) {
			doorways [doorwayID].GetComponent<DoorwayTrigger>().ExitDoorway (player);
		}
		LoadCurrentLevelProgress(scene.name);
		ApplyLevelState();
		PinholeOpen();
	}

	private void LoadOrCreateState() {
		
		if (File.Exists(Application.persistentDataPath + "/profile.plat"))
		{
			//Try to load save file
			try
			{
				Debug.Log("Loading Player Profile");
				Stream stream = File.Open(Application.persistentDataPath + "/profile.plat", FileMode.Open);
				BinaryFormatter bformatter = new BinaryFormatter();
				Profile = (PlayerProgress)bformatter.Deserialize(stream);
				Debug.Log("Loaded");
			}
			catch(Exception e)
			{
				//Create a new one
				Debug.Log("PROFILE LOAD FAILED - CREATING A NEW PROFILE");
				CreateNewProfile();
			}
		}
		else
		{
			//Create a new one
			CreateNewProfile();
		}
		
	}

	private void CreateNewProfile(bool save = true) {
		Debug.Log("Creating New Player Profile");
		Profile = new PlayerProgress();
		if (save)
		{
			SaveProfile();
		}
	}

	private void SaveProfile()
	{
		Stream stream = File.Open(Application.persistentDataPath + "/profile.plat", FileMode.Create);
		BinaryFormatter bformatter = new BinaryFormatter();
		bformatter.Serialize(stream, Profile);
		stream.Close();
		Debug.Log("Progress Saved to " + Application.persistentDataPath + "/profile.plat");
	}

	public override void DeleteProfile()
	{
		Debug.Log("DELETING PLAYER PROFILE");
		File.Delete(Application.persistentDataPath + "/profile.plat");
		CreateNewProfile(false);
		Debug.Log("DELETED");
	}

	private void LoadCurrentLevelProgress(string levelName)
	{
		CurrentLevel = LevelLoader.LoadLevel(levelName);
		if (!Profile.levelProgress.ContainsKey(levelName))
		{
			Profile.levelProgress.Add(levelName, new LevelProgress(levelName));
		}
		CurrentLevelProgress = Profile.levelProgress[levelName];
	}

	private void ApplyLevelState()
	{
		GameObject[] pips = GameObject.FindGameObjectsWithTag("Pip");
		foreach (GameObject pip in pips)
		{
			if (CurrentLevelProgress.CollectedPips.Contains(pip.GetComponent<Collectable>().id))
			{
				pip.SetActive(false);
			}
		}
		GameObject[] cartridges = GameObject.FindGameObjectsWithTag("Cartridge");
		foreach (GameObject cartridge in cartridges)
		{
			if (CurrentLevelProgress.CollectedCartridges.Contains(cartridge.GetComponent<Collectable>().id))
			{
				cartridge.SetActive(false);
			}
		}
		//TODO: FindObjectsOfType is very slow
		foreach (LevelStateObject stateObject in FindObjectsOfType(typeof(LevelStateObject)))
		{
			stateObject.ApplyState();
		}
	}

	public override int GetTotalPipsCollected() {
		int total = 0;
		foreach (LevelProgress level in Profile.levelProgress.Values) {
			total += level.CollectedPips.Count;
		}
		return total;
	}

	public override int GetTotalCartridgesCollected()
	{
		int total = 0;
		foreach (LevelProgress level in Profile.levelProgress.Values)
		{
			total += level.CollectedCartridges.Count;
		}
		return total;
	}

	public override void OnDeath()
	{
		PinholeClose(
			() =>
			{
				player.BroadcastMessage("Respawn", SendMessageOptions.DontRequireReceiver);
				doorways[CurrentLevelProgress.ActiveSpawnPoint].GetComponent<DoorwayTrigger>().ExitDoorway(player);
				PinholeOpen();
			});
	}

	public override void LoadLevel(String levelName)
	{
		doorwayID = 0;
		PinholeClose(() => {
			SceneManager.LoadScene(levelName);
		});
	}

	public override void LoadLevel(String levelName, int doorway)
	{
		doorwayID = doorway;
		PinholeClose(() => {
			SceneManager.LoadScene(levelName);
		});
	}

	private void PinholeClose(Action onComplete)
	{
		FXCameraPinhole pinhole = Camera.main.GetComponent<FXCameraPinhole>();
		pinhole.enabled = true;
		LeanTween.value(gameObject, value =>
		{
			pinhole.pinholeAmount = value;
		}, 0, 100f, 2f).setEaseInOutExpo().setOnComplete(
			() => {
				onComplete();
			});
	}

	private void PinholeOpen(bool startClosed = true)
	{
		FXCameraPinhole pinhole = Camera.main.GetComponent<FXCameraPinhole>();
		if (startClosed) {
			pinhole.pinholeAmount = 100f;
		}
		LeanTween.value(gameObject, value =>
		{
			pinhole.pinholeAmount = value;
		}, 100f, 0f, 2f).setEaseInExpo().setEaseOutQuint().setOnComplete(() =>
		{
			pinhole.enabled = false;
		});
	}

	public override void UnlockMove(string moveName)
	{
		Profile.UnlockMove(moveName);
		player.GetComponent<PlayerController>().LearnMove(moveName);
	}
}
