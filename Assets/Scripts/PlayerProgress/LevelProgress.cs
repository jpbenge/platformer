﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

[Serializable()]
public class LevelProgress : ISerializable {

	public string Name { get; private set; }

	public HashSet<string> CollectedPips { get; private set; }
	public HashSet<string> CollectedCartridges { get; private set; }

	public int ActiveSpawnPoint { get; set; }

	public Dictionary<string, int> LevelProps { get; private set; }

	public LevelProgress(string name) {
		this.Name = name;
		CollectedPips = new HashSet<string>();
		CollectedCartridges = new HashSet<string>();
		LevelProps = new Dictionary<string, int>();
		ActiveSpawnPoint = 0;
	}


	//Deserialization
	public LevelProgress(SerializationInfo info, StreamingContext ctxt)
	{
		Name = (string) info.GetValue("Name", typeof(string));
		CollectedPips = new HashSet<string>((List<string>) info.GetValue("CollectedPips", typeof(List<string>)));
		CollectedCartridges = new HashSet<string>((List<string>) info.GetValue("CollectedCartridges", typeof(List<string>)));
		ActiveSpawnPoint = (int)info.GetValue("ActiveSpawnPoint", typeof(int));
		LevelProps = (Dictionary<string, int>)info.GetValue("LevelProps", typeof(Dictionary<string, int>));
	}

	//Serialization
	public void GetObjectData(SerializationInfo info, StreamingContext context)
	{
		info.AddValue("Name", Name);
		info.AddValue("CollectedPips", CollectedPips.ToList());
		info.AddValue("CollectedCartridges", CollectedCartridges.ToList());
		info.AddValue("ActiveSpawnPoint", ActiveSpawnPoint);
		info.AddValue("LevelProps", LevelProps);
	}

	public void CollectItem(string id, Type type) {
		switch (type.Name)
		{
			case "Pip":
				CollectedPips.Add(id);
				break;
			case "Cartridge":
				CollectedCartridges.Add(id);
				break;
			default:
				throw new Exception("No Collection list for type: " + type.Name);
		}
	}
}