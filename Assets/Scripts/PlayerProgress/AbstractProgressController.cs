﻿using System;
using UnityEngine;

public abstract class AbstractProgressController : MonoBehaviour {

	public static AbstractProgressController state;

	public LevelProgress CurrentLevelProgress { get; protected set; }
	public Level CurrentLevel { get; protected set; }
	public PlayerProgress Profile { get; protected set; }
	[SerializeField]
	protected GameObject player;
	[SerializeField]
	protected bool demoMode = false;

	public virtual void DeleteProfile()
	{

	}

	public virtual int GetTotalPipsCollected()
	{
		return 0;
	}

	public virtual int GetTotalCartridgesCollected()
	{
		return 0;
	}

	public virtual void OnDeath()
	{
		
	}

	public virtual void LoadLevel(String levelName)
	{

	}

	public virtual void LoadLevel(String levelName, int doorway)
	{

	}

	public virtual void UnlockMove(string moveName)
	{

	}
}
