﻿using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class DialogueCharacter {

	[SerializeField]
	private string name;
	public string Name { get { return name; } set { name = value; } }

	[SerializeField]
	private List<DialogueNode> nodes;
	public List<DialogueNode> Nodes { get { return nodes; } set { nodes = value; } }

	public DialogueCharacter()
	{

	}

	public DialogueCharacter(string name)
	{
		this.name = name;
		Nodes = new List<DialogueNode>();
	}
}
