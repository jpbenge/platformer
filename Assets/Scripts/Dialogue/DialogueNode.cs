﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//Unity's serializer does not support abstract classes or interfaces
//so I have to use one class to act as either :(
[Serializable]
public class DialogueNode {

	public enum NodeType { Topic, BranchPoint };

	[SerializeField]
	private NodeType type;
	public NodeType Type { get { return type; } set { type = value; } }

	[SerializeField]
	private int trueNode;
	public int TrueNode { get { return trueNode; } set { trueNode = value; } }

	[SerializeField]
	private int falseNode;
	public int FalseNode { get { return falseNode; } set { falseNode = value; } }

	[SerializeField]
	private string requiredItem;
	public string RequiredItem { get { return requiredItem; } set { requiredItem = value; } }

	[SerializeField]
	private int requiredAmount;
	public int RequiredAmount { get { return requiredAmount; } set { requiredAmount = value; } }

	[SerializeField]
	private List<DialogueLine> lines;
	public List<DialogueLine> Lines { get { return lines; } set { lines = value; } }

	[SerializeField]
	private int nextNode;
	public int NextNode { get { return nextNode; } set { nextNode = value; } }

	[SerializeField]
	private string completionEvent;
	public string CompletionEvent { get { return completionEvent; } set { completionEvent = value; } }
}
