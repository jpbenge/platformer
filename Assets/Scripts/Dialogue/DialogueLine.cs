﻿using System;
using UnityEngine;

[Serializable]
public class DialogueLine {

	[SerializeField]
	private string speaker;
	public string Speaker { get { return speaker; } set { speaker = value; } }

	[SerializeField]
	private string text;
	public string Text { get { return text; } set { text = value; } }

	[SerializeField]
	private int next;
	public int Next { get { return next; } set { next = value; } }
}
