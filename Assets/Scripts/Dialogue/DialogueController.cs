﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class DialogueController : MonoBehaviour {

	public static DialogueController dialogueController;

	public DialogueNode node;
	public DialogueLine currentLine;

	private IActorInput input;
	[SerializeField]
	private Camera portraitCam;
	[SerializeField]
	private RawImage portraitImage;
	[SerializeField]
	private Vector3 playerPortraitOffset;
	private Vector3 characterPortraitOffset;
	[SerializeField]
	private Image backgroundPane;
	private float backgroundPaneInitWidth;
	[SerializeField]
	private ScrollRect dialogueScroll;
	[SerializeField]
	private Text text;
	[SerializeField]
	private float timeBetweenLetters = 0.075f;
	[SerializeField]
	private Vector3 portraitOffset;

	private GameObject characterPortrait, playerPortrait;
	private AudioClip[] characterDialogueClips, playerDialogueClips;
	private NPC npc;
	private AudioSource audioSource;
	private Vector3 portraitSpawn = Vector3.zero;

	private bool speaking = false;
	private bool skip = false;
	private Coroutine nextLineCoroutine;
	private bool waitingForNextLine = false;

	// Use this for initialization
	void Start()
	{
		dialogueController = this;
		Init();
		//if (dialogueController == null)
		//{
		//	dialogueController = this;
		//	Init();
		//}
		//else
		//{
		//	Destroy(gameObject);
		//}
	}

	public void Update() {
		if (input != null) {
			if (input.Accept() && text.text.Length > 2) {
				skip = true;
			}
		}
		if (waitingForNextLine && skip)
		{
			StopCoroutine(nextLineCoroutine);
			StartCoroutine(PlayNextLine(skip));
		}

		if (speaking)
		{
			Speak();
		}
	}

	private void Init()
	{
		audioSource = GetComponent<AudioSource>();
		backgroundPaneInitWidth = backgroundPane.rectTransform.sizeDelta.x;
		Reset();
	}

	private void Reset()
	{
		backgroundPane.enabled = false;
		text.enabled = false;
		text.text = "";
		skip = false;
	}

	private void CloseDialogueWindow()
	{
		text.text = "";
		LeanTween.size(backgroundPane.rectTransform, new Vector2(0, backgroundPane.rectTransform.rect.height), 0.5f)
			.setEaseOutExpo().setOnComplete(Reset);
	}

	public void StartDialogue(GameObject character, GameObject player, IActorInput input)
	{
		LeanTween.cancel(backgroundPane.gameObject);
		this.input = input;
		npc = character.GetComponent<NPC>();
		node = ProgressController.state.CurrentLevel.CurrentTopic(npc.NPCName, ProgressController.state.CurrentLevelProgress.LevelProps);
		if (node != null && node.Type == DialogueNode.NodeType.BranchPoint)
		{
			node = ProgressController.state.CurrentLevel.GetNextTopic(npc.NPCName, ProgressController.state.CurrentLevelProgress.LevelProps, EvaluateBranchPoint(node));
		}
		else
		{
			node = ProgressController.state.CurrentLevel.GetNextTopic(npc.NPCName, ProgressController.state.CurrentLevelProgress.LevelProps);
		}
			characterPortraitOffset = npc.PortraitOffset;
			characterDialogueClips = npc.GetDialogueClips();
			currentLine = node.Lines[0];
			backgroundPane.enabled = true;
			text.enabled = true;
			backgroundPane.rectTransform.sizeDelta = new Vector2(0, backgroundPane.rectTransform.rect.height);
			SetupPortraits(character, player);
			LeanTween.size(backgroundPane.rectTransform, new Vector2(backgroundPaneInitWidth, backgroundPane.rectTransform.rect.height), 0.75f)
			.setEaseInOutExpo();
				StartCoroutine(PlayDialogueLine(currentLine));
	}

	private void EndDialogue()
	{
		npc.ExitTalk(node);
		node = ProgressController.state.CurrentLevel.GetNextTopic(npc.NPCName, ProgressController.state.CurrentLevelProgress.LevelProps);
		Destroy(playerPortrait);
		Destroy(characterPortrait);
		CloseDialogueWindow();
	}

	private IEnumerator PlayDialogueLine(DialogueLine line)
	{
		ShowPortrait(line);

		skip = false;
		string fullLine = line.Text;
		string[] words = fullLine.Split (' ');
		int currentLetter = 0;
		int currentWord = 0;
		int lineNumber = 1;
		StringBuilder lineBuilder = new StringBuilder("");
		while (currentLetter < fullLine.Length)
		{
			if (text.cachedTextGenerator.lineCount > lineNumber) {
				lineNumber++;
				skip = false;
				if (lineNumber > 2) {
					LeanTween.value(gameObject, UpdateScroll, dialogueScroll.verticalNormalizedPosition, 0, 0.1f);
				}
			}


			if (fullLine [currentLetter] == ' ') {
				if (currentWord < words.Length - 1) {
					currentWord++;
				}
			}
			speaking = true;
			yield return DrawText(lineBuilder, fullLine, words, currentWord, currentLetter);
			text.text = lineBuilder.ToString();
			currentLetter++;
		}
		speaking = false;
		skip = false;
		if (currentLine.Next > 0)
		{
			currentLine = node.Lines[currentLine.Next];			
			nextLineCoroutine = StartCoroutine(PlayNextLine()); 
		}
		else
		{
			yield return new WaitForSeconds(1.2f);
			EndDialogue();
		}
		
	}

	private IEnumerator PlayNextLine(bool immediate = false)
	{
		if (!immediate)
		{
			waitingForNextLine = true;
			yield return new WaitForSeconds(1.2f);
			waitingForNextLine = false;
			text.text = "";
			dialogueScroll.verticalNormalizedPosition = 1;
			StartCoroutine(PlayDialogueLine(currentLine));
		}
		else
		{
			waitingForNextLine = false;
			text.text = "";
			dialogueScroll.verticalNormalizedPosition = 1;
			StartCoroutine(PlayDialogueLine(currentLine));
		}
	}

	private IEnumerator DrawText(StringBuilder lineBuilder, string fullLine, string[] words, int currentWord, int currentLetter)
	{
		bool replaceWithNewLine = false;
		if (fullLine [currentLetter] == ' ') {
			int numLines = text.cachedTextGenerator.lineCount;
			text.text = text.text + ' ' + words [currentWord];
			text.cachedTextGenerator.Populate (text.text, text.GetGenerationSettings(text.rectTransform.sizeDelta));
			if (text.cachedTextGenerator.lineCount > numLines) {
				replaceWithNewLine = true;
			}
			text.text = lineBuilder.ToString();
		}
		if (replaceWithNewLine) {
			lineBuilder.Append ('\n');
		} else {
			lineBuilder.Append (fullLine [currentLetter]);
		}
		if (!skip) {
			yield return new WaitForSeconds (timeBetweenLetters);
		}
	}


	private void UpdateScroll(float val, float ratio) {
		dialogueScroll.verticalNormalizedPosition = val;
	}

	private void ShowPortrait(DialogueLine line)
	{
		characterPortrait.SetActive(false);
		playerPortrait.SetActive(false);

		if (line.Speaker.Equals("player"))
		{
			playerPortrait.SetActive(true);
		}
		else if (line.Speaker.Equals("character"))
		{
			characterPortrait.SetActive(true);
		}
	}

	private void SetupPortraits(GameObject character, GameObject player)
	{
		portraitSpawn = portraitCam.transform.position + portraitOffset;

		characterPortrait = Instantiate(character);
		PrepareClone(characterPortrait);
		characterPortrait.transform.position = characterPortrait.transform.position + characterPortraitOffset;

		playerPortrait = Instantiate(player);
		PrepareClone(playerPortrait);
		playerPortrait.transform.position = playerPortrait.transform.position + playerPortraitOffset;
	}

	private void PrepareClone(GameObject clone)
	{
		MonoBehaviour[] scripts = clone.GetComponentsInChildren<MonoBehaviour>();
		foreach (MonoBehaviour script in scripts)
		{
			script.enabled = false;
		}
		Camera[] cameras = clone.GetComponentsInChildren<Camera>();
		foreach (Camera cam in cameras)
		{
			cam.enabled = false;
		}
		Collider[] colliders = clone.GetComponentsInChildren<Collider>();
		foreach (Collider col in colliders)
		{
			col.enabled = false;
		}
		clone.layer = 8;
		Transform[] children = clone.GetComponentsInChildren<Transform>();
		foreach (Transform child in children)
		{
			child.gameObject.layer = 8;
		}

		clone.transform.position = portraitSpawn;
		clone.transform.LookAt(portraitCam.transform);
		clone.transform.localEulerAngles = new Vector3(
			0,
			clone.transform.localEulerAngles.y,
			clone.transform.localEulerAngles.z
			);
		clone.SetActive(false);
	}


	private bool EvaluateBranchPoint(DialogueNode node)
	{

		if (node.RequiredItem != null)
		{
			switch (node.RequiredItem)
			{
				case "Pip":
					if (node.RequiredAmount > 0 && ProgressController.state.GetTotalPipsCollected() >= node.RequiredAmount)
					{
						return true;
					}
					return false;
				default:
					throw new UnityException("No item matches type " + node.RequiredItem);
			}
		}
		return false;
	}

	private void Speak()
	{
		if (!audioSource.isPlaying)
		{
			if (currentLine.Speaker.Equals("player") && playerDialogueClips != null)
			{
				audioSource.clip = playerDialogueClips[Random.Range(0,playerDialogueClips.Length)];
				audioSource.PlayDelayed(Random.Range(0, 0.15f));
			}
			else if (currentLine.Speaker.Equals("character") &&  characterDialogueClips != null)
			{
				audioSource.clip = characterDialogueClips[Random.Range(0, characterDialogueClips.Length)];
				audioSource.PlayDelayed(Random.Range(0, 0.15f));
			}
		}
	}
}
