﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class LevelLoader {

	public static Level LoadLevel(string name)
	{
		TextAsset dialogueJson = Resources.Load("Dialogue/DIAL_" + name) as TextAsset;
		Dictionary<string, DialogueCharacter> dialogue = JsonUtilityHelper.FromJson<DialogueCharacter>(dialogueJson.text).ToList().ToDictionary(character => character.Name);

		return new Level(name, dialogue);
	}
}
