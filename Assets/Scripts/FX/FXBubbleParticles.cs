﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FXBubbleParticles : MonoBehaviour {

	public ParticleSystem bubbles;
	private Transform killBubblePlane;
	private bool playing;
	private Transform waterSurfacePlane;

	// Use this for initialization
	void Start () {
		playing = false;
		bubbles = GetComponent<ParticleSystem>();
		killBubblePlane = transform.GetChild(0);
	}

	public void OnEnterWater(Transform water)
	{
		bubbles.Play();
		playing = true;
		waterSurfacePlane = water;
	}

	public void OnExitWater()
	{
		playing = false;
		bubbles.Stop();
	}

	void Update()
	{
		if (playing)
		{
			killBubblePlane.position = new Vector3(waterSurfacePlane.position.x, (waterSurfacePlane.position.y + (waterSurfacePlane.localScale.y / 2.0f)), waterSurfacePlane.position.z);
		}
	}
}
