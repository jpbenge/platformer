﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarController : MonoBehaviour {

	[SerializeField]
	private PlayerController player;
	private List<GameObject> healthIconsEmpty = new List<GameObject>();
	private List<GameObject> healthIconsFilled = new List<GameObject>();

	// Use this for initialization
	void Start () {
		foreach (Transform icon in transform)
		{
			healthIconsEmpty.Add(icon.gameObject);
			healthIconsFilled.Add(icon.transform.GetChild(0).gameObject);
		}
		healthIconsEmpty.Sort((icon1, icon2) =>
		{
			return icon1.transform.localPosition.x > icon2.transform.localPosition.x
			? 1 : -1;
		});
		healthIconsFilled.Sort((icon1, icon2) =>
		{
			return icon1.transform.localPosition.x > icon2.transform.localPosition.x
			? 1 : -1;
		});
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < healthIconsEmpty.Count; i ++)
		{
			healthIconsEmpty[i].SetActive(player.MaxHealth > i);
			healthIconsFilled[i].SetActive(player.CurHealth > i);
		}
	}
}
