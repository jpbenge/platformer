﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayerTest : MonoBehaviour {

	[SerializeField]
	private int damage;

	public void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag.Equals("Actor"))
		{
			if (other.transform.parent != null )
			{
				other.transform.parent.gameObject.SendMessage("OnHit", damage, SendMessageOptions.DontRequireReceiver);
			}
			else
			{
				other.gameObject.SendMessage("OnHit", damage, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}
