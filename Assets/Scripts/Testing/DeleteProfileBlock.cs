﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteProfileBlock : MonoBehaviour {

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag.Equals("Actor"))
		{
			LeanTween.color(gameObject, Color.red, 0.2f)
				.setOnComplete(() => LeanTween.color(gameObject, Color.white, 2f));
			ProgressController.state.DeleteProfile();
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag.Equals("Actor"))
		{
			LeanTween.color(gameObject, Color.red, 0.2f)
				.setOnComplete(() => LeanTween.color(gameObject, Color.white, 2f));
			ProgressController.state.DeleteProfile();
		}
	}
}
