﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour {

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag.Equals("Actor"))
		{
			Application.Quit();
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag.Equals("Actor"))
		{
			Application.Quit();
		}
	}
}
