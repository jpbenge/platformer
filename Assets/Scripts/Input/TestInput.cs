﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInput : MonoBehaviour, IActorInput {

	public bool accept;
	public bool back;
	public bool crouch;
	public bool dive;
	public bool jump;
	public bool jumpDown;
	public float leftHorizontal;
	public float leftVertical;
	public float rightHorizontal;
	public float rightVertical;

	public bool Accept()
	{
		return accept;
	}

	public bool Back()
	{
		return back;
	}

	public bool Crouch()
	{
		return crouch;
	}

	public bool Dive()
	{
		return dive;
	}

	public bool Jump()
	{
		return jump;
	}

	public bool JumpDown()
	{
		return jumpDown;
	}

	public float LeftHorizontal()
	{
		return leftHorizontal;
	}

	public float LeftVertical()
	{
		return leftVertical;
	}

	public float RightHorizontal()
	{
		return rightHorizontal;
	}

	public float RightVertical()
	{
		return rightVertical;
	}
}
