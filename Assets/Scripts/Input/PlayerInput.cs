﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour, IActorInput
{
	public bool Accept()
	{
		return InputManager.JumpDown();
	}

	public bool Back()
	{
		return false;
	}

	public bool Dive()
	{
		return InputManager.Dive();
	}

	public bool Crouch()
	{
		return InputManager.Crouch();
	}

	public bool Jump()
	{
		return InputManager.Jump();
	}

	public bool JumpDown()
	{
		return InputManager.JumpDown();
	}

	public float LeftHorizontal()
	{
		return InputManager.LeftHorizontal();
	}

	public float LeftVertical()
	{
		return InputManager.LeftVertical();
	}

	public float RightHorizontal()
	{
		return InputManager.RightHorizontal();
	}

	public float RightVertical()
	{
		return InputManager.RightVertical();
	}
}
