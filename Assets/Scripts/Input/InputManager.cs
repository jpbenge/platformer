﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputManager {

	public enum Controller {XBOX, PS4, GENERIC, KEYBOARD};

	public static Controller controller = GetControllerType();

	private static bool controllerSet = false;
	private static string controllerName;
	
	public static Controller GetControllerType()
	{
		
		controllerSet = true;
		if (Input.GetJoystickNames().Length > 0)
		{
			controllerName = Input.GetJoystickNames()[0];
			Debug.Log(Input.GetJoystickNames()[0]);
			switch (Input.GetJoystickNames()[0])
			{
				case "Controller (Xbox One For Windows)":
				case "Controller (XBOX 360 For Windows)":
					return Controller.XBOX;
				case "Sony Computer Entertainment Wireless Controller":
					return Controller.PS4;
				default:
					return Controller.GENERIC;
			}
		}
		controllerName = "KEYBOARD";
		return Controller.KEYBOARD;
	}

	public static void CheckControllerStatus() {
		if (controllerSet == false || (Input.GetJoystickNames().Length > 0 && Input.GetJoystickNames()[0] != controllerName))
		{
			controller = GetControllerType();
		}
	}



	public static float LeftHorizontal() {
		if (controller == Controller.KEYBOARD)
		{
			return (Input.GetButton ("KeyboardLeft") ? -1f : 0)
				+ (Input.GetButton ("KeyboardRight") ? 1f : 0);
		}
		else
		{
			return Input.GetAxis("Left Stick X");
		}
	}

	public static float LeftVertical() {
		if (controller == Controller.KEYBOARD) {
			return (Input.GetButton ("KeyboardDown") ? -1f : 0)
				+ (Input.GetButton ("KeyboardUp") ? 1f : 0);
		} 
		else {
			return Input.GetAxis ("Left Stick Y");
		}
	}

	public static float RightHorizontal() {
		if (controller == Controller.PS4)
		{
			return -1f * Input.GetAxis ("Triggers");
		} else if (controller == Controller.XBOX)
		{
			return Input.GetAxis ("Right Stick X");
		}
		else if (controller == Controller.KEYBOARD)
		{
			return Input.GetAxis ("Mouse X") * 0.25f;
		}
		else 
		{
			return -1f * Input.GetAxis("Triggers");
		}
	}

	public static float RightVertical() {
		if (controller == Controller.PS4)
		{
			return Input.GetAxis ("Right Stick X");
		} else if (controller == Controller.XBOX)
		{
			return Input.GetAxis ("Right Stick Y");
		} 
		else if (controller == Controller.KEYBOARD)
		{
			return Input.GetAxis ("Mouse Y") * 0.25f;
		}
		else
		{
			return Input.GetAxis("PS4 Right Stick Y");
		}
	}

	public static bool Jump() { 
		if (controller == Controller.XBOX) {
			return Input.GetButton("A");	
		}
		else 
		{
			return Input.GetButton("B");
		}
	}

	public static bool JumpDown() {
		if (controller == Controller.XBOX) {
			return Input.GetButtonDown ("A");
		}
		else
		{
			return Input.GetButton("B");
		}
	}

	public static bool Crouch() {
		if (controller == Controller.PS4)
		{
			return Input.GetAxis ("Right Stick Y") > 0.2f;
		} 
		else if (controller == Controller.XBOX) 
		{
			return Input.GetAxis ("Triggers") < -0.2f;
		} 
		else if (controller == Controller.GENERIC) 
		{
			return Input.GetAxis ("Right Stick X") > 0.2f;
		} 
		else 
		{
			return Input.GetButton ("KeyboardE");
		}
	}

	public static bool Dive()
	{
		if (controller == Controller.XBOX)
		{
			return Input.GetButton("X");
		}
		return Input.GetButton("X");
	}
}