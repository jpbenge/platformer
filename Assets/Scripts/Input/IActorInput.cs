﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActorInput {

	float LeftHorizontal();

	float LeftVertical();

	float RightHorizontal();

	float RightVertical();

	bool Jump();

	bool JumpDown();

	bool Crouch();

	bool Accept();

	bool Back();

	bool Dive();
}
