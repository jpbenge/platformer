﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIInput : MonoBehaviour, IActorInput
{
	private NavMeshAgent agent;
	private bool moving = false;
	private Action moveCallBack;
	private Vector3 inputDirection;
	private float speedMult = 1f;

	private void Awake()
	{
		moving = false;
	}

	private void Start()
	{
		agent = GetComponent<NavMeshAgent>();
	}

	private void Update()
	{
		if (moving)
		{
			if (!agent.pathPending)
			{
				if (agent.remainingDistance <= agent.stoppingDistance)
				{
					if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
					{
						moving = false;
						
						inputDirection = Vector3.zero;
							moveCallBack();
					}
				}
			}
			inputDirection = (agent.nextPosition - transform.position).normalized * speedMult;
			inputDirection = Quaternion.Inverse(Camera.main.transform.rotation) * inputDirection;
			agent.nextPosition = transform.position;
		}
	}

	public bool Accept()
	{
		return false;
	}

	public bool Back()
	{
		return false;
	}

	public bool Dive()
	{
		return false;
	}

	public bool Crouch()
	{
		return false;
	}

	public bool Jump()
	{
		return false;
	}

	public bool JumpDown()
	{
		return false;
	}

	public float LeftHorizontal()
	{
		//return 0;
		return Mathf.Clamp (inputDirection.x, -1f, 1f);
	}

	public float LeftVertical()
	{
		//return 0;
		return Mathf.Clamp (inputDirection.z, -1f, 1f);
	}

	public float RightHorizontal()
	{
		return 0;
	}

	public float RightVertical()
	{
		return 0;
	}

	public void WalkTo(Vector3 target, Action onComplete)
	{
		WalkTo(target, 1f, onComplete);
	}

	public void WalkTo(Vector3 target, float speedMultiplier, Action onComplete)
	{
		this.speedMult = speedMultiplier;
		agent = GetComponent<NavMeshAgent>();
		agent.nextPosition = transform.position;
		agent.ResetPath();
		agent.destination = target;
		moving = true;
		moveCallBack = onComplete;
	}
}
