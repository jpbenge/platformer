﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
	public enum State {
		Idle,
		Walking,
		Crouching,
		Sliding,
		WallSliding,
		Jumping,
		Falling,
		GroundPound,
		Talking,
		Swimming,
		Underwater
	};
	public State state { get; private set; }

	public IActorInput input { get; private set; }
	private InputMode inputMode;
	private AIInput ai;
	[SerializeField]
	public Transform mainCam;

	[SerializeField]
	private SkinnedMeshRenderer characterMesh;

	private bool ready = false;
	private Action postReadyAction;

	[Header("Health")]
	[SerializeField]
	private int maxHealth = 5;
	public int MaxHealth {
		get { return maxHealth; }
	}
	private int curHealth;
	public int CurHealth {
		get { return curHealth; }
	}
	private bool invulnerable;
	[SerializeField]
	private float invulnerableTime = 4f;
	private float invulnerableStart;
	[SerializeField]
	private bool characterBlink = true;
	[SerializeField]
	private float blinkInterval = 0.5f;
	private float lastBlinkTime;

	[Header("Movement")]
	[SerializeField]
	private float runSpeed = 1f;
	[SerializeField]
	private float groundMomentum = 0.91f;
	[SerializeField]
	private float turnSpeed = 1f;
	[SerializeField]
	private float gravity = 9.8f;

	private Vector3 hitNomral;

	private NavMeshAgent agent;
	private Transform target;

	private Vector3 currentGroundNormal;
	private GameObject currentGroundObject;
	private float currentSlopeAngle;

	[Header("Edge Handling")]
	[SerializeField]
	private bool nudgePlayerOffEdges = true;
	[SerializeField]
	private float groundedRadius = 0.4f;
	[SerializeField]
	private float nudgeStrength = 1f;

	[Header("Sliding")]
	[SerializeField]
	private float maxSlopeAngle = 25f;
	[SerializeField]
	private float maxTimeBeforeSlipping = 0.5f;
	private float timeBeforeSlipping = 0.5f;
	[SerializeField]
	private float momentumMultiplier = 1f;
	private float slopeHitTime;
	private bool hittingSlope;
	[SerializeField]
	private float startSlideSpeed = 5f;
	private float slideSpeed = 5f;
	[SerializeField]
	private float slideSpeedIncrease = 0.1f;
	[SerializeField]
	private float maxSlideSpeed = 10f;
	[SerializeField]
	private float slideTurnRange = 30f;
	[SerializeField]
	private float slideTurnSpeed = 1f;
	private Vector3 slideDirection;
	private bool sliding;

	[Header("Jump")]
	[SerializeField]
	private bool canJump = true;
	[SerializeField]
	private float jumpHeight = 10f;
	[SerializeField]
	private float jumpExtraHeight = 1f;
	[SerializeField]
	private float jumpSpeed = 1f;
	[SerializeField]
	private float jumpExtraDuration = 2f;
	private float jumpButtonDownTime;
	[SerializeField]
	private float jumpDownLeewayTime = 0.2f;
	private bool jumpReachedApex = false;
	private float jumpApex;
	private float jumpStartTime;
	[SerializeField]
	private float jumpTimeOut = 1f;
	[SerializeField]
	private float inAirMomentum = 0.99f;
	[SerializeField]
	private float jumpDI = 0.5f;

	[Header("Swimming On Surface")]
	[SerializeField]
	private float bouyancy = 12f;
	[SerializeField]
	private float maxUpwardFloat = 10f;
	[SerializeField]
	[Tooltip("How much speed is reduced when the player hits water (0 - 1)")]
	[Range(0,1)]
	private float surfaceDamping = 0.5f;
	[SerializeField]
	private float surfaceDistance = 1f;
	[SerializeField]
	private float surfaceSwimSpeed = 1f;
	[SerializeField]
	private float swimMomentum = 1f;
	private bool inWater = false;
	private bool inDeepWater = false;
	private Transform waterVolume;

	//[Header("Swimming Underwater")]
	private bool dive = false;


	[Header("Ground Pound")]
	[SerializeField]
	private bool canGroundPound = true;
	[SerializeField]
	private float groundPoundDownwardSpeed = 100f;
	private bool poundFrame = false;
	[SerializeField]
	ParticleSystem groundPoundParticles;

	private Vector3 potential = Vector3.zero;
	private bool basing = false;
	private float unBaseFrame;
	private bool unBaseStart;
	[SerializeField]
	private float basingLeewayTime = 0.1f;

	private Animator animator;
	private CharacterController controller;

	private const float DELTA_TIME_MULTIPLIER = 10f;

	private float movementInput;
	private float movementAmount;
	private Vector3 targetMovementVector;
	private Vector3 internalMovement;
	private Vector3 speed;
	private Vector3 lastPosition;
	private float leanAmount;

	private float h;
	private float v;
	private float turn;
	private bool jumpStart;
	private bool jumpDown;
	private bool jumpHold;
	private bool jumping;

	private bool crouch;

	void Awake()
	{
		//Application.targetFrameRate = 24;
	}

	void Start()
	{
		ApplyLearnedMoves();
		Respawn();
		agent = GetComponent<NavMeshAgent>();
		agent.updatePosition = false;
		agent.updateRotation = false;
		agent.updateUpAxis = false;
		agent.enabled = false;
		animator = GetComponent<Animator>();
		controller = GetComponent<CharacterController>();
		if (mainCam == null) {
			mainCam = Camera.main.transform;
		}
		internalMovement = Vector3.zero;
		if (this.inputMode != InputMode.TEST)
		{
			this.SetInputMode(InputMode.PLAYER);
		}
		ready = true;
		if (postReadyAction != null) {
			postReadyAction ();
		}
		lastPosition = transform.position;
	}

	private void ApplyLearnedMoves()
	{
		foreach (string move in AbstractProgressController.state.Profile.learnedMoves)
		{
			LearnMove(move);
		}
	}

	void Update()
	{
		GetInput();
		CheckWaterDepth();
		ApplyGravity();
		ApplyBouyancy();
		HandleCrouching();
		HandleMovement();
		HandleSliding();
		HandleWallSliding();
		HandleJump();
		HandleDive();
		HandleGroundPound();
		ApplyEdgeNudge();
		ApplyMotion();
		UpdateIFrames();
		UpdateAnimations();
		ResetPotential();
	}

	private void LateUpdate()
	{
		speed = new Vector3(Mathf.Abs(transform.position.x - lastPosition.x)
			, Mathf.Abs(transform.position.y - lastPosition.y)
			, Mathf.Abs(transform.position.z - lastPosition.z));
		lastPosition = transform.position;
	}


	private void GetInput()
	{
		h = input.LeftHorizontal();
		v = input.LeftVertical();
		turn = input.RightHorizontal();
		jumpDown = input.JumpDown();
		jumpHold = input.Jump();
		crouch = input.Crouch();
		dive = input.Dive();
	}

	private void HandleCrouching() 
	{
		if (!inDeepWater && crouch && (state.IsOneOf(State.Walking, State.Idle)))
		{
			state = State.Crouching;
		}
	}

	private void HandleMovement()
	{
		if (!sliding && state != State.Talking) {
			movementInput = Mathf.Clamp (Mathf.Abs (h) + Mathf.Abs (v), 0, 1);
			if (state != State.Swimming && state != State.WallSliding && controller.isGrounded) {
				if (state == State.Crouching) {
					targetMovementVector = Vector3.zero;
				}
				//Starting to slide
				//Slow the player down according to how long they've been trying to
				//walk on a slope, and how steep the slope is
				else if (hittingSlope && state != State.Sliding && state != State.WallSliding && Time.time > slopeHitTime && Time.time < slopeHitTime + timeBeforeSlipping)
				{
					targetMovementVector = ((mainCam.forward * v) + (mainCam.right * h)).normalized * movementInput * (runSpeed * DELTA_TIME_MULTIPLIER)
						* (1 - ((Time.time - slopeHitTime) / timeBeforeSlipping)
							* ((currentSlopeAngle - maxSlopeAngle) * 0.02f));

				}
				else //Regular Movement
				{
					targetMovementVector = ((mainCam.forward * v) + (mainCam.right * h)).normalized * movementInput * (runSpeed * DELTA_TIME_MULTIPLIER);
				}
				float movementX;
				float movementZ;
				movementX = Mathf.Lerp (internalMovement.x, targetMovementVector.x, (1f - groundMomentum));
				movementZ = Mathf.Lerp (internalMovement.z, targetMovementVector.z, (1f - groundMomentum));
				internalMovement = new Vector3 (movementX, internalMovement.y, movementZ);
				movementAmount = Mathf.Clamp (Mathf.Abs (internalMovement.x) + Mathf.Abs (internalMovement.z), 0, 1); 
			}
			//Swimming Movement
			else if (state == State.Swimming)
			{
				HandleSwimmingMovement();
			}

			//wall sliding movement
			else if (state == State.WallSliding)
			{
				targetMovementVector = ((mainCam.forward * v) + (mainCam.right * h)).normalized * movementInput * (runSpeed * DELTA_TIME_MULTIPLIER);
				targetMovementVector = new Vector3(
					currentGroundNormal.x > 0 ? Mathf.Max(1 - currentGroundNormal.x, targetMovementVector.x) : Mathf.Min(1 - currentGroundNormal.x, targetMovementVector.x),
					targetMovementVector.y,
					currentGroundNormal.z > 0 ? Mathf.Max(1 - currentGroundNormal.z, targetMovementVector.z) : Mathf.Min(1 - currentGroundNormal.z, targetMovementVector.z)
					);
				float movementX;
				float movementZ;
				movementX = Mathf.Lerp(potential.x + internalMovement.x, targetMovementVector.x, (1f - inAirMomentum));
				movementZ = Mathf.Lerp(potential.z + internalMovement.z, targetMovementVector.z, (1f - inAirMomentum));
				this.internalMovement = new Vector3(movementX, this.internalMovement.y, movementZ);
				movementAmount = Mathf.Clamp(Mathf.Abs(internalMovement.x) + Mathf.Abs(internalMovement.z), 0, 1);
			}

			//In air movement
			else {
				targetMovementVector = ((mainCam.forward * v) + (mainCam.right * h)).normalized * movementInput * (runSpeed * DELTA_TIME_MULTIPLIER);
				float movementX;
				float movementZ;
				movementX = Mathf.Lerp (potential.x + internalMovement.x, targetMovementVector.x, (1f - inAirMomentum));
				movementZ = Mathf.Lerp (potential.z + internalMovement.z, targetMovementVector.z, (1f - inAirMomentum));
				this.internalMovement = new Vector3 (movementX, this.internalMovement.y, movementZ);
				movementAmount = Mathf.Clamp (Mathf.Abs (internalMovement.x) + Mathf.Abs (internalMovement.z), 0, 1);
			}
		}
		else {
			internalMovement = new Vector3 (0, internalMovement.y, 0);
		}
	}

	private void ResetPotential()
	{
		this.potential = Vector3.zero;
	}

	private void ApplyMotion()
	{
		controller.Move(internalMovement * Time.deltaTime);

		//rotation
		Vector3 targetForwardVector;
		if (movementInput > 0)
		{
			if (basing)
			{
				targetForwardVector = new Vector3(0, Mathf.Atan2(internalMovement.x, internalMovement.z) * Mathf.Rad2Deg, 0);
			}
			else
			{
				targetForwardVector = new Vector3(0, Mathf.Atan2(targetMovementVector.x, targetMovementVector.z) * Mathf.Rad2Deg, 0);
			}
		}
		else
		{
			targetForwardVector = transform.localEulerAngles;
		}

		Vector3 rotationAmount = new Vector3(0, Mathf.DeltaAngle(transform.localEulerAngles.y, targetForwardVector.y), 0);
		if (state == State.Walking)
		{
			leanAmount = rotationAmount.y;
		}
		else // Slowly Drag the value back to zero;
		{
			leanAmount = (leanAmount > 0) ? leanAmount - Time.deltaTime : leanAmount + Time.deltaTime;
		}
		//Debug.Log(leanAmount);
		//float turnAmount = Mathf.Clamp(turnSpeed - (turnSpeed * movementAmount),0.2f, 1f);
		float turnAmount = turnSpeed;
		rotationAmount *= turnAmount * Time.deltaTime * DELTA_TIME_MULTIPLIER;
		transform.Rotate(rotationAmount);
	}

	private void ApplyEdgeNudge()
	{
		if (nudgePlayerOffEdges)
		{
			//if (controller.isGrounded && internalMovement.y <= 0)
			if (controller.isGrounded && currentGroundObject.tag != "Stairs")
			{
				RaycastHit hit;
				if (!Physics.SphereCast(transform.position + new Vector3(0, (controller.height / 2f), 0), groundedRadius, Vector3.down, out hit, controller.height / 2f))
				{
					internalMovement += new Vector3(hitNomral.x * nudgeStrength, 0f, hitNomral.z * nudgeStrength);
				}
			}
		}
	}

	private void ApplyGravity()
	{
		if (state == State.WallSliding)
		{
			//Debug.Log(HittingBelow());
		}
		if (controller.isGrounded && Vector3.Angle(Vector3.up, currentGroundNormal) < controller.slopeLimit)
		{
			basing = true;
			//internalMovement = new Vector3(internalMovement.x, internalMovement.y + (-1* gravity / TIME_MULTIPLIER), internalMovement.z);
			internalMovement = new Vector3(internalMovement.x, (-2f * gravity / DELTA_TIME_MULTIPLIER), internalMovement.z);
			if (state == State.GroundPound) {
				poundFrame = true;
			} 
			else {
				poundFrame = false;
			}
			if (state != State.Talking && state != State.Underwater) {
				state = State.Walking;
			}
		}
		else
		{
			if (jumping)
			{
				basing = false;
			}
			else if (basing)
			{
				if (!unBaseStart)
				{
					unBaseStart = true;
					unBaseFrame = Time.time;
					internalMovement = new Vector3(internalMovement.x, (-1f * gravity / DELTA_TIME_MULTIPLIER), internalMovement.z);

					if (state == State.Sliding) {
						Debug.Log("STARTING TO FALL!!!!!!");
						internalMovement = new Vector3(internalMovement.x, 0f, internalMovement.z);
					}
				}
				else if (Time.time > unBaseFrame + basingLeewayTime)
				{
					basing = false;
					unBaseStart = false;
				}
			}
			if (!basing && !inDeepWater && (state != (State.WallSliding) || controller.collisionFlags == 0))
			{
				state = State.Falling;
				potential = new Vector3(potential.x, 0, potential.z);
			}
			if (!IsSurfaced() && state != State.Underwater) {
				internalMovement = new Vector3(internalMovement.x, internalMovement.y - ((gravity * DELTA_TIME_MULTIPLIER) * Time.deltaTime), internalMovement.z);
			}
		}
	}

	private void ApplyBouyancy()
	{
		if (inDeepWater)
		{
			if (state != State.Underwater && !IsSurfaced() && transform.position.y < (waterVolume.position.y + (waterVolume.localScale.y / 2f)) - surfaceDistance)
			{
				float floatUp = Mathf.Min(internalMovement.y + (bouyancy * DELTA_TIME_MULTIPLIER * Time.deltaTime), maxUpwardFloat);
				internalMovement = new Vector3(internalMovement.x, floatUp, internalMovement.z);
			}
			else if (IsSurfaced())
			{
				state = State.Swimming;
				internalMovement = new Vector3(internalMovement.x, 0f, internalMovement.z);
			}
			else
			{
				//float floatUp = internalMovement.y + ((bouyancy * 0.5f) / TIME_MULTIPLIER);
				//internalMovement = new Vector3(internalMovement.x, floatUp, internalMovement.z);
			}
		}
	}

	private bool IsSurfaced()
	{
		return (inDeepWater
			&& (internalMovement.y >= 0f && transform.position.y >= (waterVolume.position.y + (waterVolume.transform.localScale.y / 2f)) - surfaceDistance));
	}

	private void HandleJump()
	{
		if (canJump && !sliding && state != State.WallSliding)
		{
			if (jumpDown)
			{
				jumpStart = true;
				jumpButtonDownTime = Time.time;
			}
			if (Time.time > jumpButtonDownTime + jumpDownLeewayTime)
			{
				jumpStart = false;
			}

			if (basing)
			{
				if (jumpStart)
				{
					jumping = true;
					internalMovement = new Vector3(internalMovement.x, jumpHeight + potential.y, internalMovement.z);
					jumpStartTime = Time.time;
					jumpReachedApex = false;
				}
				else
				{
					jumping = false;
				}
			}
			else if (inDeepWater && IsSurfaced())
			{
				if (jumpStart)
				{
					inDeepWater = false;
					jumping = true;
					internalMovement = new Vector3(internalMovement.x, jumpHeight + potential.y, internalMovement.z);
					jumpStartTime = Time.time;
					jumpReachedApex = false;
				}
				else
				{
					jumping = false;
				}
			}
			else
			{
				if (jumping && state != State.Talking)
				{
					state = State.Jumping;
					if (!jumpReachedApex && jumpStartTime + jumpExtraDuration > Time.time)
					{
						if (jumpHold)
						{
							internalMovement = new Vector3(internalMovement.x, internalMovement.y + (jumpExtraHeight * DELTA_TIME_MULTIPLIER * Time.deltaTime), internalMovement.z);
						}
					}
					if (!jumpReachedApex && HittingAbove())
					{
						jumpReachedApex = true;
						internalMovement = new Vector3(internalMovement.x, 0, internalMovement.z);
					}
				}
			}
		}
	}

	private void HandleSliding()
	{
		if (basing && hittingSlope 
			&& ((Time.time > slopeHitTime + timeBeforeSlipping) 
				|| Math.Abs(targetMovementVector.x) + Math.Abs(targetMovementVector.z) < 0.02f))
		{
			sliding = true;
			state = State.Sliding;
		}
		if (sliding)
		{
			internalMovement += (slideDirection * slideSpeed);
			if (slideSpeed + slideSpeedIncrease <= maxSlideSpeed)
			{
				slideSpeed += slideSpeedIncrease;
			}
			//internalMovement = new Vector3(internalMovement.x, -1, internalMovement.z);
		}
	}

	private void HandleWallSliding()
	{
		if (state == State.WallSliding)
		{
			//Debug.Log("WALLSLIDING");
			//internalMovement = new Vector3(
			//	//currentGroundNormal.x,
			//	currentGroundNormal.x > 0 ? Mathf.Max(1 - currentGroundNormal.x, internalMovement.x) : Mathf.Min(1 - currentGroundNormal.x, internalMovement.x),
			//	internalMovement.y,
			//	//currentGroundNormal.z
			//	currentGroundNormal.z > 0 ? Mathf.Max(1 - currentGroundNormal.z, internalMovement.z) : Mathf.Min(1 - currentGroundNormal.z, internalMovement.z)
			//	);
		}
	}

	private void HandleDive()
	{
		if ((IsSurfaced() && state == State.Swimming) || state == State.Underwater)
		{
			if (state == State.Underwater)
			{
				if (dive)
				{
					internalMovement = new Vector3(
						internalMovement.x,
						-5f,
						internalMovement.z);
				}
				else if (jumpHold)
				{
					internalMovement = new Vector3(
						internalMovement.x,
						5f,
						internalMovement.z);
				}
				else
				{
					internalMovement = new Vector3(
						internalMovement.x,
						0,
						internalMovement.z);
				}
			}
			else if (dive)
			{
				state = State.Underwater;
				internalMovement = new Vector3(
						internalMovement.x,
						-2f,
						internalMovement.z);
			}
		}
	}

	private void HandleGroundPound() {
		if ((state == State.Jumping || state == State.Falling) && canGroundPound) {
			if (crouch) {
				state = State.GroundPound;
				Vector3 stoppedMovement = new Vector3 (internalMovement.x, 0, internalMovement.z).normalized;
				internalMovement = new Vector3 (stoppedMovement.x, internalMovement.y - groundPoundDownwardSpeed, stoppedMovement.z);
			}
		}
	}



	private void UpdateAnimations()
	{
		animator.SetFloat("walk", movementInput);
		animator.SetBool("crouch", state == State.Crouching);
		animator.SetBool("jump", jumping);
		//Discard outlier values
		if (Mathf.Abs(animator.GetFloat("leanLR") - leanAmount) > 0.05f)
		{
			animator.SetFloat("leanLR", leanAmount);
		}
	}

	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		if (!inDeepWater)
		{
			hitNomral = hit.normal;
			Vector3 hitVector = GetHitVector(hit.collider);
			float angle = Vector3.Angle(Vector3.up, hitVector);
			//Have to add a tiny amount because >= doesn't seem to be working
			//Float improcision?
			angle = angle + 0.01f;
			if (HittingBelow() && angle >= maxSlopeAngle && angle < controller.slopeLimit && hit.point.y < transform.position.y + 1)
			{
				if (!hittingSlope)
				{
					slopeHitTime = Time.time;
					timeBeforeSlipping = Mathf.Clamp(momentumMultiplier * (speed.x + speed.z), 0, maxTimeBeforeSlipping);
					slideSpeed = startSlideSpeed;
				}
				hittingSlope = true;
				slideDirection = new Vector3(hitVector.x, -1f, hitVector.z);
			}
			else if (angle > controller.slopeLimit && !hittingSlope
				&& (state == State.Falling || (state == State.Jumping && internalMovement.y <= 0)))
			{
				RaycastHit groundCheckHit;
				if (!Physics.SphereCast(transform.position + new Vector3(0, (controller.height / 2f), 0), groundedRadius, Vector3.down, out groundCheckHit, controller.height / 2f))
				{
					state = State.WallSliding;
				}

			}
			else if (angle < 20f)
			{
				hittingSlope = false;
				sliding = false;
				slideDirection = Vector3.zero;
			}
			currentSlopeAngle = angle;
			currentGroundNormal = hit.normal;
			currentGroundObject = hit.gameObject;
			if (HittingBelow() && poundFrame)
			{
				hit.collider.SendMessage("OnGroundPound", SendMessageOptions.DontRequireReceiver);
				groundPoundParticles.Play();
				hit.gameObject.SendMessage("OnGroundPound", SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	public void AddPotential(Vector3 addedPotential) {
		this.potential = 
			new Vector3(
				potential.x + addedPotential.x,
				potential.y + Mathf.Max(0, addedPotential.y),
				potential.z + addedPotential.z
			);
	}

	public void OnGUI() {
		GUIStyle style = new GUIStyle ();
		style.fontSize = 32;
		GUI.Label (new Rect (200, 0, 200f, 32f), state.ToString (), style);
		GUI.Label (new Rect (200, 32f, 100f, 32f), AbstractProgressController.state.GetTotalPipsCollected().ToString(), style);
		GUI.Label (new Rect (320, 32f, 200f, 32f), AbstractProgressController.state.GetTotalCartridgesCollected().ToString(), style);
	}

	private Vector3 GetHitVector(Collider other)
	{
		RaycastHit hit;
		if (Physics.Raycast(transform.position, Vector3.down, out hit, 10f, ~(1 << 9)))
		{
			if (hit.collider == other)
			{
				return hit.normal;
			}
		}
		//if (HittingBelow())
		//{
		//	return CheckForSteepSlope(other);
		//}
		return Vector3.zero;
	}
	
	private Vector3 CheckForSteepSlope(Collider other)
	{
		Vector3 pos1 = transform.position + new Vector3(controller.radius / 2f, controller.height / 2f, 0f);
		RaycastHit hit1;
		if (Physics.Raycast(pos1, Vector3.down, out hit1, 10f, ~(1 << 9)))
		{
			return hit1.normal;
		}
		Vector3 pos2 = transform.position + new Vector3(-1 * controller.radius / 2f, controller.height / 2f, 0f);
		RaycastHit hit2;
		if (Physics.Raycast(pos1, Vector3.down, out hit2, 10f, ~(1 << 9)))
		{
			return hit2.normal;
		}
		Vector3 pos3 = transform.position + new Vector3(0f, controller.height / 2f, controller.radius / 2f);
		RaycastHit hit3;
		if (Physics.Raycast(pos1, Vector3.down, out hit3, 10f, ~(1 << 9)))
		{
			return hit3.normal;
		}
		Vector3 pos4 = transform.position + new Vector3(0f, controller.height / 2f, -1 * controller.radius / 2f);
		RaycastHit hit4;
		if (Physics.Raycast(pos1, Vector3.down, out hit4, 10f, ~(1 << 9)))
		{
			return hit4.normal;
		}
		return Vector3.zero;
	} 

	private bool HittingBelow() {
		return (CollisionFlags.Below & controller.collisionFlags) == CollisionFlags.Below;
	}

	private bool HittingAbove()
	{
		return (CollisionFlags.Above & controller.collisionFlags) == CollisionFlags.Above;
	}

	public void SpeakingPrompt(bool speaking)
	{
		if (basing)
		{
			canJump = !speaking;
		}
	}

	public void SetInputMode(InputMode mode)
	{
		this.inputMode = mode;
		if (this.inputMode == InputMode.PLAYER)
		{
			input = GetComponent<PlayerInput>();
		}
		else if (this.inputMode == InputMode.AI)
		{
			input = GetComponent<AIInput>();
			ai = (AIInput) input;
		}
		else if (this.inputMode == InputMode.TEST)
		{
			input = gameObject.AddComponent(typeof(TestInput)) as IActorInput;
		}
	}

	public void Talk() {
		state = State.Talking;
	}

	public void ExitTalk()
	{
		state = State.Idle;
	}

	public void LearnMove(string move)
	{
		switch (move) {
			case "GroundPound":
				canGroundPound = true;
				break;
			default:
				break;
		}
	}

	public void HandleSwimmingMovement()
	{
		targetMovementVector = ((mainCam.forward * v) + (mainCam.right * h)).normalized * movementInput * (surfaceSwimSpeed * DELTA_TIME_MULTIPLIER);
		
		float movementX;
		float movementZ;
		movementX = Mathf.Lerp(internalMovement.x, targetMovementVector.x, (1f - swimMomentum));
		movementZ = Mathf.Lerp(internalMovement.z, targetMovementVector.z, (1f - swimMomentum));
		this.internalMovement = new Vector3(movementX, this.internalMovement.y, movementZ);
		movementAmount = Mathf.Clamp(Mathf.Abs(internalMovement.x) + Mathf.Abs(internalMovement.z), 0, 1);
	}

	public void OnEnterWater(Transform waterVolume)
	{
		inWater = true;
		this.waterVolume = waterVolume;
	}

	public void OnExitWater()
	{
		inWater = false;
		inDeepWater = false;
		waterVolume = null;
		//internalMovement = new Vector3(internalMovement.x, 0f, internalMovement.z);
	}

	public void CheckWaterDepth()
	{
		if (inWater)
		{
			if (!inDeepWater && transform.position.y <= (waterVolume.position.y + (waterVolume.localScale.y / 2f)) - surfaceDistance)
			{
				inDeepWater = true;
				state = State.Swimming;
				internalMovement = internalMovement * ((1f - surfaceDamping));
			}
			//else if (transform.position.y > (waterVolume.position.y + (waterVolume.localScale.y / 2f)) - surfaceDistance)
			//{
			//	inDeepWater = false;
			//}
		}
	}

	public void OnDeath()
	{
		
	}

	public void Respawn()
	{
		internalMovement = Vector3.zero;
		curHealth = MaxHealth;
		invulnerable = false;
	}

	public void EnterDoorway(DoorwayTrigger doorway)
	{
		this.SetInputMode(InputMode.AI);
		agent.enabled = true;
		mainCam.GetComponent<PlayerCamera> ().enabled = false;
		LeanTween.move (mainCam.gameObject, doorway.CameraPoint, 0.4f);
		LeanTween.rotate(mainCam.gameObject, doorway.transform.eulerAngles, 0.4f);
		
		ai.WalkTo(doorway.WalkInTarget, () =>
		{
				if (doorway.OtherLevel != null) {
					Debug.Log(state.GetType().Name);
					AbstractProgressController.state.LoadLevel(doorway.OtherLevel, doorway.DoorWayID);
				}
		});
	}

	public void ExitDoorway(DoorwayTrigger doorway) {
		Action exitMethod = () => {
			this.SetInputMode (InputMode.AI);
			agent.enabled = false;
			gameObject.transform.position = doorway.WalkInTarget;
			agent.enabled = true;
			mainCam.GetComponent<PlayerCamera> ().enabled = false;
			mainCam.transform.position = doorway.CameraPoint;
			mainCam.transform.eulerAngles = doorway.transform.eulerAngles;
			ai.WalkTo (doorway.WalkOutTarget, doorway.WalkSpeed, () => {
				doorway.EnableCollision();
				ReEnablePlayerInput();
			});
		};
		if (!ready) {
			postReadyAction = exitMethod;
		} 
		else {
			exitMethod ();
		}
	}

	private void ReEnablePlayerInput()
	{
		this.SetInputMode(InputMode.PLAYER);
		mainCam.GetComponent<PlayerCamera>().enabled = true;
	}

	public void OnHit(int damage)
	{
		if (!invulnerable)
		{
			curHealth -= damage;
			if (CurHealth <= 0)
			{
				AbstractProgressController.state.OnDeath();
			}
			else
			{
				invulnerable = true;
				invulnerableStart = Time.time;
				characterMesh.enabled = false;
				lastBlinkTime = Time.time;
			}
			
		}
	}

	private void UpdateIFrames()
	{
		if (invulnerable)
		{
			if (characterBlink && Time.time >= lastBlinkTime + blinkInterval)
			{
				characterMesh.enabled = !characterMesh.enabled;
				lastBlinkTime = Time.time;
			}
			if (Time.time >= invulnerableStart + invulnerableTime)
			{
				invulnerable = false;
				characterMesh.enabled = true;
			}
		}
	}
}

public static class Extensions
{
	public static bool IsOneOf(this PlayerController.State state, params PlayerController.State[] states)
	{
		foreach (PlayerController.State option in states)
		{
			if (state == option)
			{
				return true;
			}
		}
		return false;
	}
	public static bool IsNoneOf(this PlayerController.State state, params PlayerController.State[] states)
	{
		return !state.IsOneOf(states);
	}
}
