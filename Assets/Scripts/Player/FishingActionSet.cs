﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingActionSet : ScriptableObject, IActionSet {

	[SerializeField]
	private GameObject fishingRod;

	private PlayerController playerController;
	private Animator playerAnimator;

	public void OnAttach(PlayerController playerController, Animator playerAnimator) {
		this.playerController = playerController;
		this.playerAnimator = playerAnimator;
		//Attach / show fishing rod
	}

	public void OnDestroy() {
		//Detach / hide fishing rod
	}

	public void PrimaryAction() {
		//Attack With Fishing Rod
		
	}

	public void SecondaryAction() {
		//Enter Fishing rod aim mode

	}
}