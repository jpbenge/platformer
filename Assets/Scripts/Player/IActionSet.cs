﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActionSet {

	void OnAttach(PlayerController playerController, Animator playerAnimator);
	void OnDestroy();
	void PrimaryAction();
	void SecondaryAction();
}
