﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level
{
	public string Name { get; private set; }
	public Dictionary<string, DialogueCharacter> Dialogue { get; private set; }

	public Level(string name, Dictionary<string, DialogueCharacter> dialogue)
	{
		Name = name;
		Dialogue = dialogue;
	}

	public DialogueNode CurrentTopic(string characterName, Dictionary<string, int> levelProps)
	{
		if (levelProps.ContainsKey(characterName))
		{
			return Dialogue[characterName].Nodes[levelProps[characterName]];
		}
		else
		{
			return null;
		}
	}


	public DialogueNode GetNextTopic(string characterName, Dictionary<string, int> levelProps, bool passedCheck = true)
	{
		if (!levelProps.ContainsKey(characterName))
		{
			levelProps[characterName] = 0;
		}
		else
		{
			if (passedCheck)
			{
				levelProps[characterName] = Dialogue[characterName].Nodes[levelProps[characterName]].NextNode;
			}
			else
			{
				levelProps[characterName] = Dialogue[characterName].Nodes[levelProps[characterName]].FalseNode;
			}
		}
		return  Dialogue[characterName].Nodes[levelProps[characterName]];
	}
}
