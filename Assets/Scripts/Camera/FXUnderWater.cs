﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FXUnderWater : MonoBehaviour {

	//new private Camera camera;
	[SerializeField]
	private Material effect;
	private Camera cam;
	public bool submerged = false;

	private void Awake()
	{
		cam = GetComponent<Camera>();
		cam.depthTextureMode = DepthTextureMode.Depth;
	}

	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if (submerged && effect != null)
		{
			Graphics.Blit(source, destination, effect);
		}
		else
		{
			Graphics.Blit(source, destination);
		}
	}

	public void OnEnterWater(Transform waterVolume)
	{
		submerged = true;
	}

	public void OnExitWater()
	{
		submerged = false;
	}

	// Use this for initialization
	//void Start () {
	//	Debug.Log("0.0.6");
	//	camera = GetComponent<Camera>();
	//	gameObject.AddComponent<MeshFilter>();
	//	gameObject.AddComponent<MeshRenderer>();
	//}

	// Update is called once per frame
	//void Update () {
	//	Vector3[] frustumCorners = new Vector3[4];
	//	camera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), camera.nearClipPlane + 0.1f, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);
	//	Mesh mesh = new Mesh();
	//	mesh.vertices = frustumCorners;
	//	Vector2[] uvs = new Vector2[4];
	//	float ratio = (frustumCorners[2].x - frustumCorners[0].x) / (frustumCorners[2].y - frustumCorners[0].y);
	//	uvs[0] = new Vector2(0, 0);
	//	uvs[1] = new Vector2(0, 1f / camera.aspect);
	//	uvs[2] = new Vector2(1f, 1f / camera.aspect);
	//	uvs[3] = new Vector2(1f, 0);
	//	mesh.uv = uvs;
	//	int[] tris = new int[6];
	//	tris[0] = 0;
	//	tris[1] = 1;
	//	tris[2] = 2;
	//	tris[3] = 0;
	//	tris[4] = 2;
	//	tris[5] = 3;
	//	mesh.triangles = tris;
	//	mesh.RecalculateNormals();
	//	GetComponent<MeshRenderer>().material = mat;
	//	GetComponent<MeshFilter>().mesh = mesh;

	//	Collider[] volumes = Physics.OverlapBox(
	//		transform.position
	//		, new Vector3((frustumCorners[2].x - frustumCorners[0].x), (frustumCorners[2].y - frustumCorners[0].y), (frustumCorners[2].z - frustumCorners[0].z))
	//		, Quaternion.identity
	//		);
	//	foreach (Collider col in volumes)
	//	{
	//		if (col.gameObject.layer == 4)
	//		{

	//		}
	//	}
	//	}
}
