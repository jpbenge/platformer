﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FXCameraPinhole : MonoBehaviour {

	[SerializeField]
    private Material effect;

	[Range(0, 100f)]
	public float pinholeAmount = 0;

    void OnRenderImage(RenderTexture source, RenderTexture destination) {
		if (effect != null)
		{
			effect.SetFloat("_PinholeAmount", pinholeAmount);
			Graphics.Blit(source, destination, effect);
		}
    }
}
