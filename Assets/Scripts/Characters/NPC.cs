﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

	[SerializeField]
	private string npcName;
	[SerializeField]
	private AudioClip[] dialogueClips;

	public string NPCName
	{
		get { return npcName; }
		private set { npcName = value; }
	}
	[SerializeField]
	private Vector3 portraitOffset;
	public Vector3 PortraitOffset
	{
		get { return portraitOffset; }
		private set { portraitOffset = value; }
	}

	[SerializeField]
	private GameObject talkIndicator;

	private PlayerController playerController;
	private IActorInput input;

	private bool inRange = false;
	private bool talking = false;

	private Camera mainCam, convoCam;

	// Use this for initialization
	void Start () {
		mainCam = Camera.main;
		convoCam = GetComponentInChildren<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if (inRange && !talking)
		{
			if (input.Accept())
			{
				Talk();
			}
		}
	}

	public void OnTriggerStay(Collider other)
	{
		if (!talking && other.gameObject.tag == "Actor")
		{
			float angle = Vector3.Angle(other.transform.forward, (transform.position - other.transform.position).normalized);
			if (angle < 80f)
			{
				SetInRange(other.gameObject, true);
			}
			else
			{
				SetInRange(other.gameObject, false);
			}
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Actor")
		{
			SetInRange(other.gameObject, false);
		}
	}


	private void SetInRange(GameObject actor, bool inRange)
	{
		this.inRange = inRange;
		talkIndicator.SetActive(inRange);
		playerController = actor.GetComponent<PlayerController> ();
		playerController.SpeakingPrompt(inRange);
		this.input = actor.GetComponent<PlayerController>().input;
	}

	public void Talk()
	{
		talking = true;
		mainCam.enabled = false;
		convoCam.enabled = true;
		talkIndicator.SetActive (false);
		playerController.Talk ();
		DialogueController.dialogueController.StartDialogue(gameObject, playerController.gameObject, input);
	}

	public void ExitTalk(DialogueNode node)
	{
		if (node.CompletionEvent != null)
		{
			gameObject.GetComponent<DialogueCallback>().OnCompletionEvent(node.CompletionEvent);
		}
		mainCam.enabled = true;
		convoCam.enabled = false;
		playerController.ExitTalk();
		talking = false;
		SetInRange(playerController.gameObject, inRange);
	}

	public AudioClip[] GetDialogueClips()
	{
		return dialogueClips;
	}
}
