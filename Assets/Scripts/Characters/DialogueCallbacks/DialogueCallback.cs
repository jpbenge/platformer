﻿using UnityEngine;

public abstract class DialogueCallback : MonoBehaviour {

	public virtual void OnCompletionEvent(string completionEvent) {

	}
}
