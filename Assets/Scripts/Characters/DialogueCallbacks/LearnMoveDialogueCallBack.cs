﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LearnMoveDialogueCallBack : DialogueCallback {

	[SerializeField]
	private PlayerController player;

	public override void OnCompletionEvent(string completionEvent)
	{
		switch (completionEvent)
		{
			case "LearnGroundPound":
				ProgressController.state.UnlockMove("GroundPound");
				break;
			default:
				break;
		}
	}
}
