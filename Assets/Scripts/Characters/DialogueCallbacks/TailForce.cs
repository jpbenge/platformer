﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConstantForce))]
public class TailForce : MonoBehaviour {

	[SerializeField]
	private Transform rootMovement;
	[SerializeField]
	private float forceMultiplier = 1f;

	private ConstantForce force;

	private Vector3 lastPos;

	private void Start()
	{
		force = GetComponent<ConstantForce>();
	}

	private void FixedUpdate()
	{
		if (lastPos != null)
		{
			force.force = -1 * (rootMovement.position - lastPos) * forceMultiplier;
		}

		lastPos = rootMovement.transform.position;
	}
}
