﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerHeadTracking : MonoBehaviour {

	[SerializeField]
	private float maxDistance = 100f;
	[SerializeField]
	private float maxTurnAngle = 75f;


	private float lookAtWeight = 0;

	private Animator animator;

	private List<KeyValuePair<Transform, HeadTrackingTarget>> targets = new List<KeyValuePair<Transform, HeadTrackingTarget>>();

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		HeadTrackingTarget[] trackingTargets = Object.FindObjectsOfType<HeadTrackingTarget>();
		foreach (HeadTrackingTarget target in trackingTargets)
		{
			targets.Add(new KeyValuePair<Transform, HeadTrackingTarget>(target.GetLookAtTarget(), target));
		}
	}

	void Update()
	{
		if (targets.Count > 0)
		{
			targets.RemoveAll(target => target.Key == null);
			targets.Sort((target1, target2) => (Vector3.Distance(transform.position, target1.Key.position) / target1.Value.influence)
			.CompareTo((Vector3.Distance(transform.position, target2.Key.position) / target2.Value.influence)));
		}
	}

	private void OnAnimatorIK(int layerIndex)
	{
		if (targets.Count > 0)
		{
			if (Vector3.Distance(transform.position, targets[0].Key.position) < maxDistance)
			{
				float angle = Vector3.Angle(transform.forward, (targets[0].Key.position - transform.position).normalized);
				if (angle < maxTurnAngle)
				{
					if (lookAtWeight < 1f)
					{
						lookAtWeight += 0.05f;
					}
				}
				else
				{
					if (lookAtWeight > 0f)
					{
						lookAtWeight -= 0.05f;
					}
				}
				animator.SetLookAtWeight(lookAtWeight);
			}
			else
			{
				if (lookAtWeight < 1f)
				{
					lookAtWeight += 0.05f;
				}
			}
			animator.SetLookAtPosition(targets[0].Key.position);
		}
	}
}
