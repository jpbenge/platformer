﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicControllerSlopeTest : MonoBehaviour {

	public IActorInput input { get; private set; }
	private float h;
	private float v;
	private bool jumpDown;
	private bool jumpHold;
	private Vector3 hitNormal;
	CharacterController controller;
	private Vector3 move;
	bool hitting;
	// Use this for initialization
	void Start () {
		InputManager.CheckControllerStatus();
		input = GetComponent<PlayerInput>();
		controller = GetComponent<CharacterController>();
	}

	// Update is called once per frame
	void Update () {
		h = input.LeftHorizontal();
		v = input.LeftVertical();
		jumpDown = input.JumpDown();
		jumpHold = input.Jump();

		move = new Vector3(h * 10f, move.y, v * 10f);

		if (jumpDown)
		{
			move = new Vector3(move.x, 0f, move.z);
		}
		if (jumpHold)
		{
			move += new Vector3(0, 15f, 0);
		}
		else if (hitting)
		{
			move += new Vector3(hitNormal.x * 2f, -1 * 1 * 10f, hitNormal.z * 2f);
			//+Debug.Log("hitNormal: " + hitNormal + " || " + hitNormal * -10f);
		}
		else
		{
			move += new Vector3(0, -10f, 0);
		}

		controller.Move(move * Time.deltaTime);
	}

	private void OnControllerColliderHit(ControllerColliderHit hit)
	{
		float angle = Vector3.Angle(Vector3.up, hit.normal);
		hitting = true;
		hitNormal = hit.normal;
	}
}
