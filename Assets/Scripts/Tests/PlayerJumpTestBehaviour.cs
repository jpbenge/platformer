﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;

public class PlayerJumpTestBehaviour : MonoBehaviour, IMonoBehaviourTest{

	private PlayerJumpTestConfig config;
	private GameObject ground;
	private GameObject player;
	private PlayerController playerController;
	private GameObject playerCamera;
	private GameObject progressController;

	private TestInput input;

	private bool hitGround = false;
	private bool testDone;
	private float highestYValue;
	private bool hasJumped = false;

	public void Start()
	{
		config = (PlayerJumpTestConfig)Resources.Load("Testing/PlayerJumpTestConfig");
		ground = config.ground;
		player = config.player;
		playerCamera = config.playerCamera;
		progressController = config.progressController;
	
		ground = GameObject.Instantiate(ground);
		player = GameObject.Instantiate(player, new Vector3(0, 5f, 0), Quaternion.identity);
		playerCamera = GameObject.Instantiate(playerCamera);
		playerCamera.GetComponent<PlayerCamera>().target = player.transform;
		progressController = GameObject.Instantiate(progressController);
		player.GetComponent<PlayerController>().SetInputMode(InputMode.TEST);
		input = player.GetComponent<TestInput>();
		highestYValue = -999f;
	}

	public bool IsTestFinished
	{
		get
		{
			return Time.time > 10f || testDone;
		}
	}

	public void Update()
	{
		if (player.GetComponent<CharacterController>().isGrounded)
		{
			if (!hitGround)
			{
				hitGround = true;
				Assert.Greater(Time.time, 0.73f);
				Assert.Less(Time.time, 0.78f);
			}

			input.jumpDown = true;
			input.jump = true;
		}
		else
		{
			if (input.jumpDown)
			{
				hasJumped = true;
			}
			input.jumpDown = false;
		}
		if (hasJumped && player.transform.position.y > highestYValue)
		{
			highestYValue = player.transform.position.y;
			
		}
		else if (hasJumped && !testDone)
		{
			Debug.Log(highestYValue);
			//Assert.Greater(highestYValue, 3.33f);
			//Assert.Less(highestYValue, 3.40f);
			testDone = true;
		}
	}
}
