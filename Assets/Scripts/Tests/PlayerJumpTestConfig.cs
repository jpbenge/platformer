﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerJumpTestConfig : ScriptableObject {

	public GameObject ground;
	public GameObject player;
	public GameObject playerCamera;
	public GameObject progressController;
}
