﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

[ExecuteInEditMode]
[RequireComponent(typeof(Collider))]
public abstract class Collectable : MonoBehaviour {

	[SerializeField]
	private string guid;
	public string id
	{
		get
		{
			return guid;
		}
		private set
		{
			guid = value;
		}
	}

	public void OnEnable()
	{
		#if UNITY_EDITOR
		if (!Application.isPlaying && (guid == null || guid.Length < 1))
		{
			guid = Guid.NewGuid().ToString();
			EditorUtility.SetDirty(this);
			EditorSceneManager.MarkSceneDirty(gameObject.scene);
		}
		#endif

	}

	public void OnTriggerEnter(Collider other) {
		if (other.gameObject.layer == 9)
		{
			ProgressController.state.CurrentLevelProgress.CollectItem(guid, this.GetType());
			Destroy(gameObject);
		}
	}
}
