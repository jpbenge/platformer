﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableSpinBounce : MonoBehaviour {

	[SerializeField] private bool bob = true;
	[SerializeField] private float bobAmount = 1f;
	[SerializeField] private bool spin = true;
	[SerializeField] private float spinSpeed = 1f;
	[SerializeField] private bool rock = true;
	[SerializeField] private float rockAmount = 1f;

	private const float TIME_MULTIPLIER = 10f;

	private float initialHieght;
	private float initialXRotation;

	void Start () {
		initialHieght = transform.position.y - (bobAmount / 2f);
		initialXRotation = transform.localEulerAngles.x - (rockAmount / 2f);
	}

	// Update is called once per frame
	void Update () {
		if (bob) {
			transform.position = new Vector3 (transform.position.x, (initialHieght + (bobAmount * Mathf.Sin (Time.time))), transform.position.z);
		}
		if (spin) {
			transform.Rotate(new Vector3(0f, spinSpeed * Time.deltaTime * TIME_MULTIPLIER, 0f));
		}
		if (rock) {
			float rotateX = Mathf.Sin (Time.time) * rockAmount;
			transform.localEulerAngles = new Vector3 (rotateX, transform.localEulerAngles.y, transform.localEulerAngles.z);
		}
	}
}
