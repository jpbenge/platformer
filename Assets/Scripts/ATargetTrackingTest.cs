﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ATargetTrackingTest : MonoBehaviour {


	public Transform target;
	public float targetSpeed = 10f;
	public Vector3 targetMovementNormal = Vector3.zero;
	public Vector3 targetVelocity = Vector3.zero;
	public Vector3 thisPosition = Vector3.zero;
	public float projectileSpeed = 10f;
	public Vector3 firingSolution = Vector3.zero;

	public Transform bullet;
	// Use this for initialization
	void Start () {
		thisPosition = transform.position;
		targetMovementNormal = new Vector3(Random.Range(-100f, 100f), Random.Range(-100f, 100f), Random.Range(-100f, 100f)).normalized;
		targetVelocity = targetMovementNormal * targetSpeed;
		firingSolution = (CalculateLead() - transform.position).normalized * projectileSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		target.position += targetVelocity;
		bullet.position += firingSolution;
	}

	Vector3 CalculateLead()
	{
		Vector3 V = targetVelocity;
		Vector3 D = target.position - thisPosition;
		float A = V.sqrMagnitude - projectileSpeed * projectileSpeed;
		float B = 2 * Vector3.Dot(D, V);
		float C = D.sqrMagnitude;
		if (A >= 0)
		{
			Debug.LogError("No solution exists");
			return target.position;
		}
		else
		{
			float rt = Mathf.Sqrt(B * B - 4 * A * C);
			float dt1 = (-B + rt) / (2 * A);
			float dt2 = (-B - rt) / (2 * A);
			float dt = (dt1 < 0 ? dt2 : dt1);
			return target.position + V * dt;
		}
	}
}
