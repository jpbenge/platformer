﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : GenericMovingPlatform {

	[Header("Motion")]
	[SerializeField]
	private bool moveX = false;
	[SerializeField]
	private float amountX = 1f;
	[SerializeField]
	private float speedX = 1f;
	[SerializeField]
	private bool moveY = false;
	[SerializeField]
	private float amountY = 1f;
	[SerializeField]
	private float speedY = 1f;
	[SerializeField]
	private bool moveZ = false;
	[SerializeField]
	private float amountZ = 1f;
	[SerializeField]
	private float speedZ = 1f;

	[Header("Rotation")]
	[SerializeField]
	private Vector3 spinSpeed = Vector3.zero;

	private float x, y, z;

	private const float TIME_MULTIPLIER = 10f;

	private Vector3 initialPosition;

	protected override void Start()
	{
		base.Start();
		initialPosition = transform.position;
		x = initialPosition.x;
		y = initialPosition.y;
		z = initialPosition.z;
	}

	void OnDrawGizmos()
	{
		Vector3 origin = initialPosition != Vector3.zero ? initialPosition: transform.position;
		if (moveX)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(origin - new Vector3(amountX, 0, 0), origin + new Vector3(amountX, 0, 0));
		}
		if (moveY)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawLine(origin - new Vector3(0,amountY, 0), origin + new Vector3(0,amountY, 0));
		}
		if (moveZ)
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(origin - new Vector3(0, 0, amountZ), origin + new Vector3(0, 0, amountZ));
		}
	}

	protected override void Update()
	{
		if (moveX)
		{
			x = initialPosition.x + amountX * Mathf.Sin(Time.time * speedX);
		}
		if (moveY)
		{
			y = initialPosition.y + amountY * Mathf.Sin(Time.time * speedY);
		}
		if (moveZ)
		{
			z = initialPosition.z + amountZ * Mathf.Sin(Time.time * speedZ);
		}

		transform.position = new Vector3(x, y, z);
		if (spinSpeed != Vector3.zero)
		{
			transform.localEulerAngles += (new Vector3(spinSpeed.x * Time.deltaTime * TIME_MULTIPLIER,
				spinSpeed.y * Time.deltaTime * TIME_MULTIPLIER,
				spinSpeed.z * Time.deltaTime * TIME_MULTIPLIER));
		}
		base.Update();
	}
}
