﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadTrackingTarget : MonoBehaviour {

	public float influence = 1;

	[SerializeField]
	private Transform targetTransform;
	
	public Transform GetLookAtTarget()
	{
		return targetTransform != null ? targetTransform : transform;
	}
}
