﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WaterVolume : MonoBehaviour {

	void OnDrawGizmos()
	{
		Gizmos.color = new Color(0.4f, 0.6f, 0.8f, 0.6f);
		Gizmos.DrawCube(GetComponent<Collider>().bounds.center, GetComponent<Collider>().bounds.size);
	}


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		other.gameObject.BroadcastMessage("OnEnterWater", transform ,  SendMessageOptions.DontRequireReceiver);
	}

	void OnTriggerExit(Collider other)
	{
		other.gameObject.BroadcastMessage("OnExitWater", SendMessageOptions.DontRequireReceiver);
	}
}
