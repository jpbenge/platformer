﻿using UnityEngine;

public abstract class LevelStateObject : MonoBehaviour {

	public abstract void ApplyState();

	public abstract void SaveState(int state);
}
