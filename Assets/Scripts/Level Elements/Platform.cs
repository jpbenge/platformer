﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{

	protected List<GameObject> actors = new List<GameObject>();

	void OnCollisionEnter(Collision collision)
	{
		if (collision.transform.parent != null)
		{
			AddToMoveList(collision.transform.parent.gameObject);
		}
		else
		{
			AddToMoveList(collision.gameObject);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.transform.parent != null)
		{
			AddToMoveList(other.transform.parent.gameObject);
		}
		else
		{
			AddToMoveList(other.gameObject);
		}
	}

	void OnCollisionExit(Collision collision)
	{
		if (collision.transform.parent != null)
		{
			RemoveFromList(collision.transform.parent.gameObject);
		}
		else
		{
			RemoveFromList(collision.gameObject);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.transform.parent != null)
		{
			RemoveFromList(other.transform.parent.gameObject);
		}
		else
		{
			RemoveFromList(other.gameObject);
		}
	}

	void AddToMoveList(GameObject gameObject)
	{
		if (gameObject.tag.Equals("Actor") && !actors.Contains(gameObject))
		{
			actors.Add(gameObject);
		}
	}

	void RemoveFromList(GameObject gameObject)
	{
		if (actors.Contains(gameObject))
		{
			actors.Remove(gameObject);
		}
	}
}