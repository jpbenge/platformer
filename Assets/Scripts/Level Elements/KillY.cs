﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillY : MonoBehaviour {

	[SerializeField]
	private GameObject spawnPoint;

	void OnDrawGizmos()
	{
		Gizmos.color = new Color(0.8f, 0.6f, 0.6f, 0.5f);
		Gizmos.DrawCube(transform.position, transform.localScale);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag.Equals("Actor"))
		{
			//other.transform.position = spawnPoint.transform.position;
			ProgressController.state.OnDeath();
		}
	}
}
