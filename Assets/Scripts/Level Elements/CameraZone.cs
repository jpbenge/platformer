﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Utility;
using UnityEngine;

public class CameraZone : MonoBehaviour {

	[SerializeField]
	private float tweenSpeed = 0.5f;
	[SerializeField]
	private Color gizmoColor;
	[SerializeField]
	private Transform contextCamPosition;
	[SerializeField]
	private Camera mainCam;
	private bool inContest;

	private Vector3 startScale;

	void OnDrawGizmos()
	{
		Gizmos.color = gizmoColor;
		Gizmos.DrawCube(transform.position, transform.localScale);
	}

	void Start()
	{
		if (contextCamPosition.GetComponent<Camera>() != null) {
			contextCamPosition.GetComponent<Camera>().enabled = false;
		}
		startScale = mainCam.transform.localScale;
	}

	void OnTriggerEnter(Collider other)
	{
		if (!inContest && other.gameObject.tag.Equals("Actor"))
		{
			inContest = true;
			contextCamPosition.GetComponent<SmoothFollow>().enabled = true;
			mainCam.GetComponent<PlayerCamera>().enabled = false;
			//mainCam.GetComponent<SmoothFollow>().enabled = true;
			LeanTween.move(mainCam.gameObject, contextCamPosition, tweenSpeed).setEaseInOutCubic();
			LeanTween.rotate(mainCam.gameObject, contextCamPosition.localEulerAngles, tweenSpeed).setEaseInOutCubic()
				.setOnComplete(() => {
					mainCam.transform.parent = contextCamPosition;
				});
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (inContest && other.gameObject.tag.Equals("Actor"))
		{
			inContest = false;
			contextCamPosition.GetComponent<SmoothFollow>().enabled = false;
			mainCam.transform.parent = null;
			mainCam.transform.SetPositionAndRotation(contextCamPosition.position, contextCamPosition.rotation);
			mainCam.transform.localScale = startScale;
			mainCam.GetComponent<SmoothFollow>().enabled = false;
			mainCam.GetComponent<PlayerCamera>().enabled = true;
		}
	}

}
