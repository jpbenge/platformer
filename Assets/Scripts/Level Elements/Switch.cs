﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Switch : MonoBehaviour {

	public bool Down { get; protected set; }

	public virtual void TurnOff()
	{

	}
}
