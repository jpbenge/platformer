﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericMovingPlatform : Platform {

	[SerializeField]
	protected Transform motionPivotPoint;
	protected Vector3 lastPosition;
	protected Quaternion lastRotation;

	protected virtual void Start()
	{
		motionPivotPoint = motionPivotPoint != null ? motionPivotPoint : transform;
		lastPosition = motionPivotPoint.position;
		lastRotation = motionPivotPoint.localRotation;
	}

	protected virtual void Update() {
		Vector3 motionDelta = motionPivotPoint.position - lastPosition;
		float angleDelta = Quaternion.Angle(motionPivotPoint.localRotation, lastRotation);
		actors.ForEach(actor =>
		{
			if (true)
			//if (actor.GetComponent<CharacterController>().isGrounded)
			{
				//Move Actor
				actor.transform.position = actor.transform.position + (motionDelta);
				//Add potential energy
				//deviding out deltaTime because that gets multiplied in all at once
				actor.GetComponent<PlayerController>().AddPotential(((motionDelta) / Time.deltaTime));
				//Rotate Actor
				actor.transform.RotateAround(motionPivotPoint.position, Vector3.up, angleDelta);
			}
		});
		lastPosition = motionPivotPoint.position;
		lastRotation = motionPivotPoint.localRotation;
	}
}