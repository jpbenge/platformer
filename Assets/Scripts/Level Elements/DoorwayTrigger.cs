﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DoorwayTrigger : MonoBehaviour {

	[SerializeField]
	private int id = 0;
	public int Id
	{
		get
		{
			return id;
		}
		private set
		{
			id = value;
		}
	}

	[Header("Link to Another Level")]
	[SerializeField]
	private string otherLevel;
	public string OtherLevel
	{
		get {
			return otherLevel;
		}
		private set{
			otherLevel = value;
		}
	}
	[SerializeField]
	private int doorwayID = 0;
	public int DoorWayID {
		get {
			return doorwayID;
		}
		private set {
			doorwayID = value;
		}
	}

	[Header("Link to Doorway in this Level")]
	[SerializeField]
	private GameObject otherDoorway;
	public GameObject OtherDoorway
	{
		get
		{
			return otherDoorway;
		}
		private set
		{
			otherDoorway = value;
		}
	}

	[SerializeField]
	private Vector3 relativeDoorCamPos = new Vector3(0, 2.5f, -8f);
	public Vector3 CameraPoint {
		get {
			return transform.position + transform.rotation * relativeDoorCamPos;
		}
		private set {
			CameraPoint = value;
		}
	}

	[SerializeField]
	private Vector3 relativeWalkToPoint = new Vector3(0, 0, 4);
	public Vector3 WalkInTarget {
		get
		{
			return transform.position + transform.rotation * relativeWalkToPoint;
		}
		private set
		{
			relativeWalkToPoint = value;
		}
	}

	[SerializeField]
	private Vector3 relativeWalkOutPoint = new Vector3(0, 0, -5);
	public Vector3 WalkOutTarget {
		get
		{
			return transform.position + transform.rotation * relativeWalkOutPoint;
		}
		private set
		{
			relativeWalkOutPoint = value;
		}
	}

	[SerializeField]
	private float walkSpeed = 1f;
	public float WalkSpeed
	{
		get
		{
			return walkSpeed;
		}
		private set
		{
			walkSpeed = value;
		}
	}

	private bool canCollide = true;

	private void Awake()
	{
		canCollide = true;
		if (OtherLevel != null && OtherDoorway != null)
		{
			throw new UnityException("Doorway must link to a level or another doorway, not both.");
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag.Equals("Actor") && canCollide) {
			canCollide = false;
			other.SendMessage("EnterDoorway", this);
		}
	}

	public void EnableCollision()
	{
		canCollide = true;
	}

	public void ExitDoorway(GameObject player) {
		canCollide = false;
		player.SendMessage ("ExitDoorway", this);
	}

	void OnDrawGizmos()
	{
		Gizmos.DrawSphere(CameraPoint, 0.5f);
		Gizmos.DrawSphere(WalkInTarget, 0.5f);
		Gizmos.DrawSphere(WalkOutTarget, 0.5f);
	}
}
