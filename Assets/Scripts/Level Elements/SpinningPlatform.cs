﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningPlatform : Platform {

	[SerializeField]
	private bool spin = true;
	[SerializeField]
	private float spinSpeed = 1f;
	private const float TIME_MULTIPLIER = 10f;

	void Update()
	{
		if (spin)
		{
			transform.Rotate(new Vector3(0f, spinSpeed * Time.deltaTime * TIME_MULTIPLIER, 0f));
			actors.ForEach(actor => actor.transform.RotateAround(transform.position, Vector3.up, spinSpeed * Time.deltaTime * TIME_MULTIPLIER));
			actors.ForEach (actor => {
				Transform tempTransform = new GameObject().transform;
				tempTransform.position = actor.transform.position;
				tempTransform.rotation = actor.transform.rotation;
				tempTransform.localScale = actor.transform.localScale;
				tempTransform.RotateAround(transform.position, Vector3.up, spinSpeed * Time.deltaTime * TIME_MULTIPLIER);
				Vector3 distance = (tempTransform.position - actor.transform.position);
				actor.GetComponent<PlayerController>().AddPotential(distance / Time.deltaTime);
				//actor.GetComponent<PlayerController>().AddMovement(distance / Time.deltaTime);
				actor.transform.localEulerAngles = tempTransform.localEulerAngles;
			});
		}
	}
}
