﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchGroupTrigger : LevelStateObject {

	[SerializeField]
	private Switch[] switches;

	[SerializeField]
	private String levelProp;

	[SerializeField]
	private float openDistanceY = 10f;

	[SerializeField]
	private float openTime = 2f;

	private Vector3 startPos;

	private bool open = false;

	private void Awake()
	{
		startPos = transform.position;
	}

	public void OnSwitchActivate(Switch activated)
	{
		if (!open)
		{
			bool allDown = true;
			foreach (Switch s in switches)
			{
				if (!s.Down)
				{
					allDown = false;
				}
			}
			if (allDown)
			{
				LeanTween.moveY(gameObject, transform.position.y - openDistanceY, openTime)
					.setEaseInCubic().setEaseOutBounce();
				foreach (Switch s in switches)
				{
					s.TurnOff();
				}
				open = true;
				SaveState(1);
			}
		}
	}

	public override void ApplyState()
	{
		if (ProgressController.state.CurrentLevelProgress.LevelProps.ContainsKey(levelProp))
		{
			int state = ProgressController.state.CurrentLevelProgress.LevelProps[levelProp];
			if (state == 1)
			{
				open = true;
				transform.position = new Vector3(startPos.x, startPos.y - openDistanceY, startPos.z);
				foreach (Switch s in switches)
				{
					s.TurnOff();
				}
			}
		}
	}

	public override void SaveState(int state)
	{
		ProgressController.state.CurrentLevelProgress.LevelProps[levelProp] = state;
	}
}
