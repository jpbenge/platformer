﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

	void OnDrawGizmos()
	{
		Gizmos.color = new Color(0.5f, 0.8f, 0.6f, 0.5f);
		Gizmos.DrawSphere(transform.position, 1);
	}
}
