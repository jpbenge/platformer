﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSwitch : Switch {

	[SerializeField]
	private SwitchGroupTrigger trigger;

	[SerializeField]
	private float timeLimit = 8f;
	private float downTime;
	private bool stayDown = false;

	[SerializeField]
	private float moveDistanceY = 1f;
	private Vector3 startPos;

	private void Awake()
	{
		startPos = transform.position;
	}

	public void Update()
	{
		if (Down && !stayDown && Time.time > downTime + timeLimit)
		{
			Down = false;
			LeanTween.moveY(gameObject, transform.position.y + moveDistanceY, 0.2f);
		}
	}

	public void OnGroundPound()
	{
		LeanTween.moveY(gameObject, transform.position.y - moveDistanceY, 0.2f);
		Down = true;
		trigger.OnSwitchActivate(this);
		downTime = Time.time;
	}

	public override void TurnOff ()
	{
		stayDown = true;
		transform.position = new Vector3(startPos.x,
			startPos.y - moveDistanceY, startPos.z);
	}
}
