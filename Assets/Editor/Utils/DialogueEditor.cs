﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class DialogueEditor : EditorWindow {

	private static string levelName = "NEW_LEVEL";
	private static List<DialogueCharacter> characters = new List<DialogueCharacter>();
	private static Color defaultGUIColor = new Color();
	private static Color addButtonColor = new Color(0.7f, 1f, 0.8f);
	private static Color deleteButtonColor = new Color(1f, 0.7f, 0.7f);
	private static GUIStyle subFrameStyle = new GUIStyle();
	private static float verticalScroll = 0;

	[MenuItem("Dialogue/Edit")]
	public static void ShowDialogueWindow()
	{
		subFrameStyle.margin = new RectOffset(20, 20, 10, 10);
		EditorWindow window = EditorWindow.GetWindow(typeof(DialogueEditor), false, "Edit Level Dialogue");
		window.minSize = new Vector2(900f, 900f);
		defaultGUIColor = GUI.color;
	}

	private void OnGUI()
	{
		verticalScroll = EditorGUILayout.BeginScrollView(
			new Vector2(0, verticalScroll),
			subFrameStyle,
			GUILayout.Width(860)
			).y;
		GUILayout.Label("Level", EditorStyles.boldLabel);
		
		levelName = EditorGUILayout.TextField("Level Name", levelName);
		EditorGUILayout.Space();
		GUILayout.Label("NPCs", EditorStyles.boldLabel);
		RenderNPCs();

		GUI.color = addButtonColor;
		if (GUILayout.Button("New NPC"))
		{
			characters.Add(new DialogueCharacter("NPC_" + characters.Count));
		}
		GUI.color = defaultGUIColor;
		EditorGUILayout.Space();
		if (GUILayout.Button("Save Dialogue"))
		{
			this.SaveDialogue();
		}
		if (GUILayout.Button("Load Dialogue"))
		{
			this.LoadDialogue();
		}


		EditorGUILayout.EndScrollView();
	}

	private void SaveDialogue()
	{
		if (levelName == "")
		{
			Debug.LogError("Invalid Level Name");
		}
		else
		{
			string output = JsonUtilityHelper.ToJson(characters.ToArray());
			System.IO.File.WriteAllText(Application.dataPath + "/Resources/Dialogue/DIAL_" + levelName + ".json", output);
			Debug.Log("Saved to " + Application.dataPath + "/Resources/Dialogue/DIAL_" + levelName + ".json");
		}
	}

	private void LoadDialogue()
	{
		string filePath = EditorUtility.OpenFilePanel("Load Level Dialogue", Application.dataPath + "/Resources/Dialogue", "json");
		filePath = filePath.Replace(Application.dataPath + "/Resources/", "");
		filePath = filePath.Replace(".json", "");
		TextAsset dialogueJson = Resources.Load(filePath) as TextAsset;
		characters = JsonUtilityHelper.FromJson<DialogueCharacter>(dialogueJson.text).ToList();
	}


	private void RenderNPCs() {
		List<DialogueCharacter> charactersToRemove = new List<DialogueCharacter>();
		foreach (DialogueCharacter character in characters)
		{
			EditorGUILayout.BeginVertical(subFrameStyle, GUILayout.Width(820));
			character.Name = EditorGUILayout.TextField("NPC Name", character.Name);

			this.RenderDialogueNodes(character);

			GUI.color = addButtonColor;
			if (GUILayout.Button("New Dialogue Node"))
			{
				DialogueNode nextNode = new DialogueNode();
				nextNode.Type = DialogueNode.NodeType.Topic;
				nextNode.NextNode = character.Nodes.Count;
				nextNode.Lines = new List<DialogueLine>();
				character.Nodes.Add(nextNode);
			}
			GUI.color = defaultGUIColor;

			GUI.color = deleteButtonColor;
			if (GUILayout.Button("Delete NPC"))
			{
				charactersToRemove.Add(character);
			}
			GUI.color = defaultGUIColor;
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();

		}

		foreach (DialogueCharacter character in charactersToRemove)
		{
			characters.Remove(character);
		}
	}

	private void RenderDialogueNodes(DialogueCharacter character)
	{
		List<DialogueNode> nodesToRemove = new List<DialogueNode>();
		GUILayout.Label("Nodes", EditorStyles.boldLabel);
		foreach (DialogueNode node in character.Nodes)
		{
			EditorGUILayout.BeginVertical(subFrameStyle, GUILayout.Width(780));
			GUILayout.Label("Node " + character.Nodes.IndexOf(node), EditorStyles.boldLabel);
			GUILayout.Label("Node Type", EditorStyles.boldLabel);
			if (EditorGUILayout.Toggle("Topic", node.Type == DialogueNode.NodeType.Topic, EditorStyles.radioButton))
			{
				node.Type = DialogueNode.NodeType.Topic;
			}
			if (EditorGUILayout.Toggle("BranchPoint", node.Type == DialogueNode.NodeType.BranchPoint, EditorStyles.radioButton))
			{
				node.Type = DialogueNode.NodeType.BranchPoint;
			}

			if (node.Type == DialogueNode.NodeType.Topic)
			{
				this.RenderTopic(character, node);
			}

			if (node.Type == DialogueNode.NodeType.BranchPoint)
			{
				this.RenderBranchPoint(character, node);
			}

			GUI.color = deleteButtonColor;
			if (GUILayout.Button("Delete Node"))
			{
				nodesToRemove.Add(node);
			}
			GUI.color = defaultGUIColor;
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
		}

		foreach (DialogueNode node in nodesToRemove)
		{
			character.Nodes.Remove(node);
		}
	}

	private void RenderTopic(DialogueCharacter character, DialogueNode node)
	{
		this.RenderTopicLines(character, node);

		if (character.Nodes.Count > 1)
		{
			int[] selectionValues = new int[character.Nodes.Count];
			string[] displayValues = new string[character.Nodes.Count];
			for (int i = 0; i < character.Nodes.Count; i++)
			{
				selectionValues[i] = i;
				displayValues[i] = "Node " + i;
			}
			GUILayout.Label("Next Node", EditorStyles.boldLabel);
			node.NextNode = EditorGUILayout.IntPopup(node.NextNode, displayValues, selectionValues);
		}
		else
		{
			GUILayout.Label("No other nodes to link to");
			GUILayout.Label("Next Node = self");
		}
		GUILayout.Label("Completion Event", EditorStyles.boldLabel);
		node.CompletionEvent = EditorGUILayout.TextField(node.CompletionEvent);
	}

	private void RenderTopicLines(DialogueCharacter character, DialogueNode node)
	{
		GUILayout.Label("Lines", EditorStyles.boldLabel);
		List<DialogueLine> linesToDelete = new List<DialogueLine>();
		foreach (DialogueLine line in node.Lines)
		{
			EditorGUILayout.BeginVertical(subFrameStyle, GUILayout.Width(740));
			GUILayout.Label("Speaker", EditorStyles.boldLabel);

			int speakerInt = line.Speaker == "character" ? 0 : 1;
			speakerInt = EditorGUILayout.Popup(speakerInt, new string[] { character.Name, "Player" });

			line.Speaker = speakerInt == 0 ? "character" : "player";
			
			GUILayout.Label("Text", EditorStyles.boldLabel);
			line.Text = EditorGUILayout.TextArea(line.Text, GUILayout.Height(40));

			GUI.color = deleteButtonColor;
			if (GUILayout.Button("Delete Line"))
			{
				linesToDelete.Add(line);
			}
			GUI.color = defaultGUIColor;
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
		}

		foreach (DialogueLine line in linesToDelete)
		{
			node.Lines.Remove(line);
		} 

		GUI.color = addButtonColor;
		if (GUILayout.Button("New Line"))
		{
			DialogueLine newLine = new DialogueLine();
			newLine.Speaker = character.Name;
			newLine.Text = "";
			node.Lines.Add(newLine);
		}
		GUI.color = defaultGUIColor;
	}

	private void RenderBranchPoint(DialogueCharacter character, DialogueNode node)
	{
		node.RequiredItem = EditorGUILayout.TextField("Required Item", node.RequiredItem);
		node.RequiredAmount = EditorGUILayout.IntSlider("Required Amount", node.RequiredAmount, 1, 9999);
		if (character.Nodes.Count > 1)
		{
			int[] selectionValues = new int[character.Nodes.Count];
			string[] displayValues = new string[character.Nodes.Count];
			for (int i = 0; i < character.Nodes.Count; i++)
			{
				selectionValues[i] = i;
				displayValues[i] = "Node " + i;
			}
			GUILayout.Label("True Node", EditorStyles.boldLabel);
			node.NextNode = EditorGUILayout.IntPopup(node.NextNode, displayValues, selectionValues);
			GUILayout.Label("False Node", EditorStyles.boldLabel);
			node.FalseNode = EditorGUILayout.IntPopup(node.FalseNode, displayValues, selectionValues);
		}
		else
		{
			GUILayout.Label("No other nodes to link to");
			GUILayout.Label("True Node = self");
			GUILayout.Label("False Node = self");
		}
	}
}
